package com.HostoPedia.dao;

import java.sql.Connection;
import java.util.ArrayList;

import com.HostoPedia.bean.Room;

public interface RoomAllocationDao {

//	public ArrayList<Room> selectRoom(Connection connection);

	public ArrayList<Room> selectRoom(Connection connection);

}
