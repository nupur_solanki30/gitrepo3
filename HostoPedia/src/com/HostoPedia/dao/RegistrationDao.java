/**
 * 
 */
package com.HostoPedia.dao;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;


public interface RegistrationDao {

	public int insertUserDetails(User user);

	public int insertStudentDetails(Student student);
}

