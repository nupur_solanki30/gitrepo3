package com.HostoPedia.dao;

import java.sql.Connection;
import java.util.List;

import com.HostoPedia.bean.Booking;

public interface BookingDao {
	public int saveBookingDetails(Connection connection, int roomId, int stuId, double price);
	public List<Booking> selectBookingDetails(Connection connection);
	public int editBookingDetails(Connection connection, Booking booking);
	public int saveBookingDetails(Connection connection, Booking booking);
}
