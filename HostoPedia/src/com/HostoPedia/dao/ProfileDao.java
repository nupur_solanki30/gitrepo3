package com.HostoPedia.dao;
import java.sql.Connection;
import java.sql.SQLException;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;
public interface ProfileDao {
	public int updateUserDetails(Connection connection, User user) throws SQLException;
	public int updateStudentDetails(Connection connection, Student student) throws SQLException;
}
