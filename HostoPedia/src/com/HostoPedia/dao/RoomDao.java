package com.HostoPedia.dao;

import java.sql.Connection;
import java.util.ArrayList;

import com.HostoPedia.bean.Room;

public interface RoomDao {
	public int insertRoom(Connection connection,Room room);
	public int deleteRoom(Connection connection, String  RoomId);

	public ArrayList<Room> selectRoom(Connection connection);

	public int updateRoom(Connection connection,Room room);

	public Room selectRoomDetails(Connection connection, String RoomId);
	public int getRoomCountFromUserTable(Connection connection,int roomId);
}
