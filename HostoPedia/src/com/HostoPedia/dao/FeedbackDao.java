package com.HostoPedia.dao;

import java.sql.Connection;
import java.util.List;
import com.HostoPedia.bean.Feedback;

public interface FeedbackDao {
	public int saveFeedbackDetails(Connection connection, Feedback feedBack);
	public List<Feedback> selectFeedbackDetails(Connection connection, int feedbackId);
	public List<Feedback> selectFeedbackDetails(Connection connection);
	public int deleteFeedbackDetails(Connection connection, String feedbackId);
}
