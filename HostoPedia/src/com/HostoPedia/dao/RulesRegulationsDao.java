package com.HostoPedia.dao;

import java.sql.Connection;
import java.util.ArrayList;

import com.HostoPedia.bean.RulesRegulationsBean;

public interface RulesRegulationsDao {
	public int insertRules(Connection connection, RulesRegulationsBean  rulesRegulationsBean);
	public int deleteRules(Connection connection, String  RulesId);

	public ArrayList<RulesRegulationsBean> selectRules(Connection connection);

	public int updateRules(Connection connection, RulesRegulationsBean  rulesRegulationsBean);

	public RulesRegulationsBean selectRulesDetails(Connection connection, String rulesId);
	
}
