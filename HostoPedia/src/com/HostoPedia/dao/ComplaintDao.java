package com.HostoPedia.dao;

import java.sql.Connection;
import java.util.List;

import com.HostoPedia.bean.Complaint;

public interface ComplaintDao {
	public int saveComplaintDetails(Connection connection, Complaint complaint);
	public List<Complaint> selectComplaintDetails(Connection connection, int comptId);
	public List<Complaint> selectComplaintDetails(Connection connection);
	
}
