package com.HostoPedia.dao;

import java.sql.Connection;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;

public interface LoginDao {
	User getUser(Connection connection,String email, String pass);
	Student getStudent(Connection connection, int userId);
}
