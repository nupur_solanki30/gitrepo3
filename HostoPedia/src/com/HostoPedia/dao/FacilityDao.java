package com.HostoPedia.dao;

import java.sql.Connection;
import java.util.ArrayList;
import com.HostoPedia.bean.Facility;

public interface FacilityDao {

	public int insertFacility(Connection connection, Facility  facility);
	public int deleteFacility(Connection connection, String  FacilityId);

	public ArrayList<Facility> selectFacility(Connection connection);

	public int updateFacility(Connection connection, Facility facility);

	public Facility selectFacilityDetails(Connection connection, String facilityId);
	
}
