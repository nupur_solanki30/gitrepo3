package com.HostoPedia.dao;

import java.sql.Connection;
import java.util.ArrayList;
import com.HostoPedia.bean.Gallery;

public interface GalleryDao {
	public int insertGallery(Connection connection, Gallery gallery);
	public int deleteGallery(Connection connection, String  GalleryId);

	public ArrayList<Gallery> selectGallery(Connection connection);

	public int updateGallery(Connection connection, Gallery gallery);

	public Gallery selectGalleryDetails(Connection connection, String GalleryId);
}
