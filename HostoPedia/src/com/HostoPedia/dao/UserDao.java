package com.HostoPedia.dao;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;


public interface UserDao {

	public int insertUserDetails(User user);
	
public List<User> selectUserDetails(Connection connection);
	
	//public List<Student> selectStudentDetails(Connection connection);

	public int deleteUserDetails(Connection connection, String userId);
	
	public int deleteStudentDetails(Connection connection, String userId);
	
	public User selectUserDetails(Connection connection, String userId);

	public User selectStudentDetails(Connection connection, String userId);

	public int updateUserDetails(Connection connection, User user);
	
	public int updateStudentDetails(Connection connection, Student student) throws SQLException;
	
	public ArrayList<User> selectAllUserDetails(Connection connection);
	
	public int getUser(Connection connection,String email);

	public int updatePassword(Connection connection, String password, String email);

	public List<Student> selectBookingDetails(Connection connection);
}


