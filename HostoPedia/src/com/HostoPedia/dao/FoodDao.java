package com.HostoPedia.dao;

import java.sql.Connection;
import java.util.ArrayList;

import com.HostoPedia.bean.Food;

public interface FoodDao {
	public int insertFood(Connection connection, Food food);
	public int deleteFood(Connection connection, String  FoodId);

	public ArrayList<Food> selectFood(Connection connection);

	public int updateFood(Connection connection, Food food);

	public Food selectFood(Connection connection, String foodId);
}
