package com.HostoPedia.dao;

import java.sql.Connection;
import java.util.ArrayList;


import com.HostoPedia.bean.StaffDetails;



public interface StaffDetailsDao {

	public int insertStafftDetails(Connection connection, StaffDetails staffDetails);
	public int deleteStaffDetails(Connection connection, String  StaffId);

	public ArrayList<StaffDetails> selectStaffDetails(Connection connection);

	public int updateStaffDetails(Connection connection, StaffDetails staffDetails);

	public StaffDetails selectStaffDetails(Connection connection, String staffId);
	
}
