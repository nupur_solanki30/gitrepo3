package com.HostoPedia.dao;

import java.sql.Connection;
import java.util.ArrayList;

import com.HostoPedia.bean.Event;


public interface EventDao {
	public int insertEvent(Connection connection, Event  event);
	public int deleteEvent(Connection connection, String  EventId);

	public ArrayList<Event> selectEvent(Connection connection);

	public int updateEvent(Connection connection,Event  event);

	public Event selectEventDetails(Connection connection, String EventId);
}
