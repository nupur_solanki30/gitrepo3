package com.HostoPedia.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.HostoPedia.bean.Complaint;
import com.HostoPedia.bean.Feedback;
import com.HostoPedia.dao.ComplaintDao;

public class ComplaintDaoImpl implements ComplaintDao{

	@Override
	public int saveComplaintDetails(Connection connection, Complaint complaint) {
		System.out.println("Save Comaplint Detail start");
		String insertQuery = "insert into complaint_table(Compt_name, Compt_des, Compt_date, User_id) values(?,?,?,?)";
		try(PreparedStatement preparedStatement = connection.prepareStatement(insertQuery))
		{
			preparedStatement.setString(1, complaint.getComptName());
			preparedStatement.setString(2, complaint.getComptDescription());
			preparedStatement.setString(3, complaint.getComptDate());
			preparedStatement.setInt(4, complaint.getUserId());
			return preparedStatement.executeUpdate();
			
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Save Complaint Detail end");
		return 0;
	}

	@Override
	public List<Complaint> selectComplaintDetails(Connection connection, int comptId) {
		String selectQuery="select ct.Compt_id,ct.Compt_name, ct.Compt_des,ct.Compt_date,ut.User_name from complaint_table ct, user_table ut where ct.Compt_id=? and ct.User_id=ut.User_id";
		List<Complaint> complaintList=new ArrayList<Complaint>();
		try(PreparedStatement preparedStatement=connection.prepareStatement(selectQuery))
		{
			preparedStatement.setInt(1, comptId);
			
			try(ResultSet resultSet=preparedStatement.executeQuery())
			{
				while(resultSet.next())
				{
					int complaintId=resultSet.getInt("Compt_id");
					String comptName=resultSet.getString("Compt_name");
					String comptDesc=resultSet.getString("Compt_des");
					String date=resultSet.getString("Compt_date");
					String userName=resultSet.getString("User_name");
					
					Complaint complaint = new Complaint();
					complaint.setComptId(complaintId);
					complaint.setComptName(comptName);
					complaint.setComptDescription(comptDesc);
					complaint.setComptDate(date);
					complaint.setUserName(userName);
					complaintList.add(complaint);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return complaintList;
	}

	@Override
	public List<Complaint> selectComplaintDetails(Connection connection) {
		String selectQuery="select ct.Compt_id,ct.Compt_name, ct.Compt_des,ct.Compt_date,ut.User_name from complaint_table ct, user_table ut where ct.User_id=ut.User_id";
		List<Complaint> complaintList=new ArrayList<Complaint>();
		try(PreparedStatement preparedStatement=connection.prepareStatement(selectQuery))
		{
			try(ResultSet resultSet=preparedStatement.executeQuery())
			{
				while(resultSet.next())
				{
					int complaintId=resultSet.getInt("Compt_id");
					String comptName=resultSet.getString("Compt_name");
					String comptDesc=resultSet.getString("Compt_des");
					String date=resultSet.getString("Compt_date");
					String userName=resultSet.getString("User_name");
					
					
					Complaint complaint = new Complaint();
					complaint.setComptId(complaintId);
					complaint.setComptName(comptName);
					complaint.setComptDescription(comptDesc);
					complaint.setComptDate(date);
					complaint.setUserName(userName);
					complaintList.add(complaint);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return complaintList;
	}

}
