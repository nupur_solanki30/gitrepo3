package com.HostoPedia.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;
import com.HostoPedia.dao.UserDao;



public class UserDaoImpl implements UserDao {

	public int insertUserDetails(User user) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		String insertQuery="insert into user_table(User_id, User_name, User_email_id, User_contact, User_add, User_pass, User_role) values(?,?,?,?,?,?,?)";
		String connectionUrl="jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername="root";
		String connectionPassword="root";
		int insertRow=0;
		int recordId=0;
		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername, connectionPassword);
				PreparedStatement preparedStatement=connection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS))
				{
					preparedStatement.setString(1, user.getUserName());
					preparedStatement.setString(2, user.getUserEmailid());
					preparedStatement.setString(3, user.getUserContact());
					preparedStatement.setString(4, user.getUserAdd());
					preparedStatement.setString(5, user.getUserPass());
					preparedStatement.setString(6, user.getUserRole());
					
					
					insertRow=preparedStatement.executeUpdate();
					
					ResultSet resultSet=preparedStatement.getGeneratedKeys();
					while(resultSet.next()) {
						recordId=resultSet.getInt(1);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		return recordId;
	}
	
	@Override
	public List<User> selectUserDetails(Connection connection) {
		String selectQuery = "select * from user_table";
		List<User> userList = new ArrayList<>();

		try (PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
				ResultSet resultSet = preparedStatement.executeQuery();) {

			while (resultSet.next()) {

				User user = new User();
				Integer userId = resultSet.getInt("User_id ");
				String username = resultSet.getString("User_name");
				String emailid = resultSet.getString("User_email_id");
				String usercontact = resultSet.getString("User_contact");
				String useradd = resultSet.getString("User_add");
				String userpass = resultSet.getString("User_pass");
				String userrole = resultSet.getString("User_role");
				user.setUserId(userId);
				user.setUserName(username);
				user.setUserEmailid(emailid);
				user.setUserContact(usercontact);
				user.setUserAdd(useradd);
				user.setUserPass(userpass);
				user.setUserRole(userrole);
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userList;
	}

	@Override
	public int deleteUserDetails(Connection connection, String userId) {
		// TODO Auto-generated method stub
		int userid=Integer.parseInt(userId);
		String deleteUserQuery= "delete from user_table where User_id = ?";
		try(PreparedStatement preparedStatement = connection.prepareStatement(deleteUserQuery)){
			preparedStatement.setInt(1, userid);
			return preparedStatement.executeUpdate();
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		return 0;
	}

	@Override
	public int deleteStudentDetails(Connection connection, String userId) {
		// TODO Auto-generated method stub
		System.out.println("dao layer called");
		System.out.println("delete student method called");
		
		int userid=Integer.parseInt(userId);
		String deleteUserQuery= "delete from student_table where User_id = ?";
		try(PreparedStatement preparedStatement = connection.prepareStatement(deleteUserQuery)){
			preparedStatement.setInt(1, userid);
			return preparedStatement.executeUpdate();
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		System.out.println("dao layer complted");
		System.out.println("delete student method completed");
		
		return 0;
	}

	@Override
	public User selectUserDetails(Connection connection, String userId) {
		// TODO Auto-generated method stub
		User user = new User();
		
		try(PreparedStatement preparedStatement = connection.prepareStatement("select * from user_table where User_id = ?")){
			preparedStatement.setInt(1, Integer.parseInt(userId));
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				
				user.setUserId(resultSet.getInt("User_id"));
				user.setUserName(resultSet.getString("User_name"));
				user.setUserEmailid(resultSet.getString("User_email_id"));
				user.setUserContact(resultSet.getString("User_contact"));
				user.setUserAdd(resultSet.getString("User_add"));
				user.setUserPass(resultSet.getString("User_pass"));
				user.setUserRole(resultSet.getString("User_role"));
				
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
		
		return user;
	}

	@Override
	public User selectStudentDetails(Connection connection, String userId) {
		// TODO Auto-generated method stub
		System.out.println("Dao layer called");
		System.out.println("Select student method called");
		Student student = new Student();
		
		try(PreparedStatement preparedStatement = connection.prepareStatement("select ut.User_id,st.Stu_id,st.Stu_clgname,st.Stu_Doc,st.Stu_Dob,st.Stu_fathername,st.stu_mothername,ut.User_name,ut.User_email_id,ut.User_contact,ut.User_add,ut.User_pass,ut.User_role from user_table ut, student_table st where st.User_id = ut.User_id and ut.User_id = ?")){
			preparedStatement.setInt(1, Integer.parseInt(userId));
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				
				student.setUserId(resultSet.getInt("User_id"));
				student.setStuId(resultSet.getInt("Stu_id"));
				student.setStuClgName(resultSet.getString("Stu_clgname"));
				System.out.println("student document :::" + (null!=resultSet.getBytes(("Stu_Doc"))));
				if(null!=resultSet.getBytes(("Stu_Doc"))) {
					student.setStuDocString(Base64.getEncoder().encodeToString(resultSet.getBytes(("Stu_Doc"))));
				}
				student.setStuDob(resultSet.getString("Stu_Dob"));
				student.setStuFatherName(resultSet.getString("Stu_fathername"));
				student.setStuMotherName(resultSet.getString("stu_mothername"));
				
			
				student.setUserName(resultSet.getString("User_name"));
				student.setUserEmailid(resultSet.getString("User_email_id"));
				student.setUserContact(resultSet.getString("user_contact"));
				student.setUserAdd(resultSet.getString("user_add"));
				student.setUserPass(resultSet.getString("User_pass"));
				student.setUserRole(resultSet.getString("User_role"));
				System.out.println("Email " + student.getUserEmailid());
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
		System.out.println("Dao layer completed");
		System.out.println("Select student method completed");	
		return student;
	}


	@Override
	public int updateUserDetails(Connection connection, User user) {
		// TODO Auto-generated method stub
		try(PreparedStatement preparedStatement = connection.prepareStatement("update user_table set User_name=?, User_email_id=?, User_contact=?, User_add=?, User_pass=?, User_role=? where User_id=?")){
			//preparedStatement.setInt(1, );
			preparedStatement.setString(1, user.getUserName());
			preparedStatement.setString(2, user.getUserEmailid());
			preparedStatement.setString(3, user.getUserContact());
			preparedStatement.setString(4, user.getUserAdd());
			preparedStatement.setString(5, user.getUserPass());
			preparedStatement.setString(6, user.getUserRole());
			preparedStatement.setInt(7, user.getUserId());
			return preparedStatement.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}


	@Override
	public int updateStudentDetails(Connection connection, Student student) throws SQLException {
		// TODO Auto-generated method stub
		System.out.println("Dao layer called");
		System.out.println("Update student method called");
		
		String query = "update student_table set Stu_clgname =?,Stu_fathername = ?,stu_mothername= ? "
				+ ", stu_doc = COALESCE(?,Stu_Doc) "
				+ "where User_id = ?";
//		
		int counter = 1;
		try(PreparedStatement ps = connection.prepareStatement(query)){
			
			ps.setString(counter++, student.getStuClgName());
			ps.setString(counter++, student.getStuFatherName());
			ps.setString(counter++, student.getStuMotherName());
			ps.setBlob(counter++, student.getStuDocStream());
//			ps.setString(counter++,student.getStuDob());
			ps.setInt(counter++, student.getUserId());

		//	ps.setString(7, student.getUserRole());
			System.out.println("Dao layer completed");
			System.out.println("update student method completed");
			return ps.executeUpdate();
		}
	}				
			
	

	@Override
	public ArrayList<User> selectAllUserDetails(Connection connection) {
		// TODO Auto-generated method stub
		String selectQuery = "select * from user_table";
		ArrayList<User> userList = new ArrayList<>();

		try (PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
				ResultSet resultSet = preparedStatement.executeQuery();) {

			while (resultSet.next()) {

				User user = new User();
				int userId = resultSet.getInt("User_id");
				String username = resultSet.getString("User_name");
				String emailid = resultSet.getString("User_email_id");
				String usercontact = resultSet.getString("User_contact");
				String useradd = resultSet.getString("User_add");
				String userpass = resultSet.getString("User_pass");
				String userrole = resultSet.getString("User_role");
				user.setUserId(userId);
				user.setUserName(username);
				user.setUserEmailid(emailid);
				user.setUserContact(usercontact);
				user.setUserAdd(useradd);
				user.setUserPass(userpass);
				user.setUserRole(userrole);
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userList;
	}

	@Override
	public int getUser(Connection connection, String email) {
		// TODO Auto-generated method stub
		System.out.println("inside getUser Dao layer");
		String forgetPassQuery = "Select count(*) from user_table where User_email_id = ?";
		int selectRow = 0;
		try (PreparedStatement preparedStatement = connection.prepareStatement(forgetPassQuery)){
				preparedStatement.setString(1,email);
				try(ResultSet resultSet = preparedStatement.executeQuery();){
					while(resultSet.next()) {
						selectRow = resultSet.getInt(1);
						System.out.println("Get user dao layer end  ");
						System.out.println(selectRow);
				
						return selectRow;
					}
				}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return 0;
	}

	@Override
	public int updatePassword(Connection connection, String password, String email) {
		// TODO Auto-generated method stub
		String updateQuery="update user_table set User_pass=? where User_email_id=?";
		try(PreparedStatement preparedStatement=connection.prepareStatement(updateQuery))
		{
			preparedStatement.setString(1, password);
			preparedStatement.setString(2, email);
			return preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
		
	}

	@Override
	public List<Student> selectBookingDetails(Connection connection) {
		String selectQuery = "select * from student_table";
		List<Student> studentList = new ArrayList<>();

		try (PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
				ResultSet resultSet = preparedStatement.executeQuery();) {

			while (resultSet.next()) {
				Student student = new Student();
				//Integer stuId
			//	student.setUserId(resultSet.getInt("User_id"));
				student.setStuId(resultSet.getInt("Stu_id"));
				student.setStuClgName(resultSet.getString("Stu_clgname"));
				System.out.println("student document :::" + (null!=resultSet.getBytes(("Stu_Doc"))));
				if(null!=resultSet.getBytes(("Stu_Doc"))) {
					student.setStuDocString(Base64.getEncoder().encodeToString(resultSet.getBytes(("Stu_Doc"))));
				}
				student.setStuDob(resultSet.getString("Stu_Dob"));
				student.setStuFatherName(resultSet.getString("Stu_fathername"));
				student.setStuMotherName(resultSet.getString("stu_mothername"));
				student.setRoomId(resultSet.getInt("Room_id"));
				
				

//				User user = new User();
//				Integer userId = resultSet.getInt("User_id ");
//				String username = resultSet.getString("User_name");
//				String emailid = resultSet.getString("User_email_id");
//				String usercontact = resultSet.getString("User_contact");
//				String useradd = resultSet.getString("User_add");
//				String userpass = resultSet.getString("User_pass");
//				String userrole = resultSet.getString("User_role");
//				user.setUserId(userId);
//				user.setUserName(username);
//				user.setUserEmailid(emailid);
//				user.setUserContact(usercontact);
//				user.setUserAdd(useradd);
//				user.setUserPass(userpass);
//				user.setUserRole(userrole);
//				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return studentList;
	}
}
