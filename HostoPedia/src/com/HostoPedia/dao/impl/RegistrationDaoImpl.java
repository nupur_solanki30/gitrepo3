package com.HostoPedia.dao.impl;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;
import com.HostoPedia.dao.RegistrationDao;



public class RegistrationDaoImpl implements RegistrationDao {

	@Override
	public int insertUserDetails(User user) {
		// TODO Auto-generated method stub
System.out.println("Dao layer called");
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";
		String insertQuery = "insert into user_table(User_name, User_email_id, User_contact, User_add, User_pass, User_role) values (?,?,?,?,?,?)";
		
		int recordId=0;
		int i = 0;
		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername, connectionPassword);
				PreparedStatement preparedStatement = connection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS)) {
			
			preparedStatement.setString(1, user.getUserName());
			preparedStatement.setString(2, user.getUserEmailid());
			preparedStatement.setString(3, user.getUserContact());
			preparedStatement.setString(4, user.getUserAdd());
			preparedStatement.setString(5, user.getUserPass());
			preparedStatement.setString(6, user.getUserRole());
			
			i = preparedStatement.executeUpdate(); 
		
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			while(resultSet.next()) {
					recordId = resultSet.getInt(1);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Dao execution complete");
		return recordId;
	}


	@Override
	public int insertStudentDetails(Student student) {
		// TODO Auto-generated method stub
		System.out.println("clgname"+student.getStuClgName());

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";
		String insertQuery = "insert into student_table(User_id, Stu_clgname, Stu_Doc, Stu_Dob, Stu_fathername, stu_mothername) values (?,?,?,?,?,?)";
		
		int recordId=0;
		int i = 0;
		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername, connectionPassword);
				PreparedStatement preparedStatement = connection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS)) {
			
			preparedStatement.setInt(1, student.getUserId() );
			preparedStatement.setString(2, student.getStuClgName());
			preparedStatement.setBlob(3, student.getStuDocStream());
			preparedStatement.setString(4, student.getStuDob());
			preparedStatement.setString(5, student.getStuFatherName());
			preparedStatement.setString(6, student.getStuMotherName());
			
			i = preparedStatement.executeUpdate(); 
		
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			while(resultSet.next()) {
					recordId = resultSet.getInt(1);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Dao execution complete");
		return recordId;
	}
}	

	
	

	




