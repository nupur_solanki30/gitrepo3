package com.HostoPedia.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;
import com.HostoPedia.dao.LoginDao;

public class LoginDaoImpl implements LoginDao{

	@Override
	public User getUser(Connection connection, String email, String pass) {
		String loginQuery = "select * from user_table where User_email_id=? and User_pass=?";
		User user = null;

		System.out.println("Login dao called");
		System.out.println("Get user method called");

		
		try (PreparedStatement preparedStatement = connection.prepareStatement(loginQuery);) {
			preparedStatement.setString(1, email);
			preparedStatement.setString(2, pass);
			try(ResultSet resultSet = preparedStatement.executeQuery();) {
				while(resultSet.next()) {
					user = new User();
					String userName = resultSet.getString("User_name");
					String userEmail = resultSet.getString("User_email_id");
					String userContact = resultSet.getString("User_contact");
					String userAddress = resultSet.getString("User_add");
					String userPassword = resultSet.getString("User_pass");
					String userRole = resultSet.getString("User_role");
					Integer userId=resultSet.getInt("User_id");
					
					user.setUserName(userName);
					user.setUserEmailid(userEmail);
					user.setUserContact(userContact);
					user.setUserAdd(userAddress);
					user.setUserPass(userPassword);
					user.setUserRole(userRole);
					user.setUserId(userId);
					return user;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Get user method completed");
		System.out.println("Login dao completed");
		return user;
	}

	@Override
	public Student getStudent(Connection connection, int userId) {
		String loginQuery = "select ut.User_id,st.Stu_id,st.Stu_clgname,st.Stu_Doc,st.Stu_Dob,st.Stu_fathername,st.stu_mothername,ut.User_name,ut.User_email_id,ut.User_contact,ut.User_add from user_table ut, student_table st where st.User_id = ut.User_id and ut.User_id = ?";
		Student student = new Student();
		
		System.out.println("Login dao called");
		System.out.println("Get student method called");


		try (PreparedStatement preparedStatement = connection.prepareStatement(loginQuery);) {
			preparedStatement.setInt(1, userId);
			try(ResultSet resultSet = preparedStatement.executeQuery();) {
				while(resultSet.next()) {
					
					student.setUserId(resultSet.getInt("User_id"));
					student.setStuId(resultSet.getInt("Stu_id"));
					student.setStuClgName(resultSet.getString("Stu_clgname"));
					System.out.println("student document :::" + (null!=resultSet.getBytes(("Stu_Doc"))));
					if(null!=resultSet.getBytes(("Stu_Doc"))) {
						student.setStuDocString(Base64.getEncoder().encodeToString(resultSet.getBytes(("Stu_Doc"))));
					}
					student.setStuDob(resultSet.getString("Stu_Dob"));
					student.setStuFatherName(resultSet.getString("Stu_fathername"));
					student.setStuMotherName(resultSet.getString("stu_mothername"));
					
					student.setUserName(resultSet.getString("User_name"));
					student.setUserEmailid(resultSet.getString("User_email_id"));
					student.setUserContact(resultSet.getString("User_contact"));
					student.setUserAdd(resultSet.getString("User_add"));
					//student.setUserPass(resultSet.getString("User_pass"));
					//student.setUserRole(resultSet.getString("User_role"));
					System.out.println("Email " + student.getUserEmailid());
					
				}
			}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Get student method completed");
		System.out.println("Login dao completed");
		return student;
	}

}
