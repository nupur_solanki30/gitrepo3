package com.HostoPedia.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.HostoPedia.bean.RulesRegulationsBean;
import com.HostoPedia.dao.RulesRegulationsDao;

public class RulesRegulationsDaoImpl implements RulesRegulationsDao {

	@Override
	public int insertRules(Connection connection,RulesRegulationsBean  rulesRegulationsBean) {
		System.out.println("rules dao impl start");
		String insertQuery="insert into rulesregulations_table(Description, Apply_from) values(?,?)";
		
		int i=0;
		int recordId=0;
		try (PreparedStatement preparedStatement=connection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS))
				{
					preparedStatement.setString(1,rulesRegulationsBean.getDescription());
					preparedStatement.setString(2,rulesRegulationsBean.getApplyDate());
					
					i = preparedStatement.executeUpdate();
					
					ResultSet resultSet = preparedStatement.getGeneratedKeys();
					while(resultSet.next()) {
							recordId = resultSet.getInt(1);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		System.out.println("rules dao impl finish");
		return recordId;
	}

	@Override
	public int deleteRules(Connection connection, String  RulesId) {
		// TODO Auto-generated method stub
		System.out.println("Dao layer called");
		System.out.println("Delete rules method called");
		int rulesid=Integer.parseInt(RulesId);
		String deleteRulesQuery= "delete from rulesregulations_table where  Rules_id = ?";
		try(PreparedStatement preparedStatement = connection.prepareStatement(deleteRulesQuery)){
			preparedStatement.setInt(1, rulesid);
			return preparedStatement.executeUpdate();
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		System.out.println("Delete rules method completed");
		System.out.println("Dao layer completed");
		return 0;
	
	}

	
	@Override
	public ArrayList<RulesRegulationsBean> selectRules(Connection connection) {
		// TODO Auto-generated method stub
		
		ArrayList<RulesRegulationsBean> rulesList = new ArrayList<>();
		try(PreparedStatement preparedStatement = connection.prepareStatement("select * from rulesregulations_table");
				ResultSet resultSet = preparedStatement.executeQuery();){
			
			while(resultSet.next()) {
				RulesRegulationsBean rulesRegulationsBean = new RulesRegulationsBean();
				
				rulesRegulationsBean.setRulesId(resultSet.getInt("Rules_Id"));
				rulesRegulationsBean.setDescription(resultSet.getString("Description"));
				rulesRegulationsBean.setApplyDate(resultSet.getString("Apply_from"));
				rulesList.add(rulesRegulationsBean);
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
		
		return rulesList;
	}


	@Override
	public int updateRules(Connection connection, RulesRegulationsBean rulesRegulationsBean) {
		// TODO Auto-generated method stub
		try(PreparedStatement preparedStatement = connection.prepareStatement("update  rulesregulations_table set Rules_id=?, Description=?, Apply_from=? where Rules_id =?")){
			
			preparedStatement.setInt(1,rulesRegulationsBean.getRulesId());
			preparedStatement.setString(2,rulesRegulationsBean.getDescription());
			preparedStatement.setString(3,rulesRegulationsBean.getApplyDate());
			preparedStatement.setInt(4,rulesRegulationsBean.getRulesId());
			return preparedStatement.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public RulesRegulationsBean selectRulesDetails(Connection connection, String rulesId) {
		
		RulesRegulationsBean rulesRegulationsBean = new RulesRegulationsBean();
		
		try(PreparedStatement preparedStatement = connection.prepareStatement("select * from rulesregulations_table where Rules_id = ?")){
			preparedStatement.setInt(1, Integer.parseInt(rulesId));
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {

				rulesRegulationsBean.setDescription(resultSet.getString("Description"));
				rulesRegulationsBean.setApplyDate(resultSet.getString("Apply_from"));
				
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
		
		return rulesRegulationsBean;
	}
}
	/*

	@Override
	public int deleteRules(Connection connection, RulesRegulationsBean rulesRegulationsBean) {
		// TODO Auto-generated method stub
		int rulesid=Integer.parseInt( RulesId);
		String deleteUserQuery= "delete from rulesregulations_table where  RulesId = ?";
		try(PreparedStatement preparedStatement = connection.prepareStatement(deleteRulesQuery)){
			preparedStatement.setInt(1, rulesid);
			return preparedStatement.executeUpdate();
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		return 0;
	
	}
}
			
*/


	
