package com.HostoPedia.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;

import com.HostoPedia.bean.Event;
import com.HostoPedia.bean.Facility;
import com.HostoPedia.dao.EventDao;

public class EventDaoImpl implements EventDao{

	@Override
	public int insertEvent(Connection connection, Event event) {
		// TODO Auto-generated method stub
		System.out.println("event dao impl start");
		String insertQuery="insert into eventdetails_table(Event_id, Event_img, Event_name, Event_date, Description) values(?,?,?,?,?)";
		
		int i=0;
		int recordId=0;
		try (PreparedStatement preparedStatement=connection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS))
				{
					preparedStatement.setInt(1,event.getEventId());
					preparedStatement.setBlob(2,event.getEventStream());
					preparedStatement.setString(3,event.getEventName());
					preparedStatement.setString(4,event.getEventDate());
					preparedStatement.setString(5,event.getEventDesc());
					
					i = preparedStatement.executeUpdate();
					
					ResultSet resultSet = preparedStatement.getGeneratedKeys();
					while(resultSet.next()) {
							recordId = resultSet.getInt(1);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		System.out.println("event dao impl finish");
		return recordId;
		
	}

	@Override
	public int deleteEvent(Connection connection, String EventId) {
		System.out.println("Dao layer called");
		System.out.println("Delete event method called");
		int eventid=Integer.parseInt(EventId);
		String deleteEventQuery= "delete from eventdetails_table where Event_id = ?";
		try(PreparedStatement preparedStatement = connection.prepareStatement(deleteEventQuery)){
			preparedStatement.setInt(1, eventid);
			return preparedStatement.executeUpdate();
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		System.out.println("Delete Event method completed");
		System.out.println("Dao layer completed");
		return 0;
	}

	@Override
	public ArrayList<Event> selectEvent(Connection connection) {
		ArrayList<Event> eventList = new ArrayList<>();
		try(PreparedStatement preparedStatement = connection.prepareStatement("select * from eventdetails_table");
				ResultSet resultSet = preparedStatement.executeQuery();){
			
			while(resultSet.next()) {
				Event event = new Event();
				
				event.setEventId(resultSet.getInt("Event_id"));
				System.out.println("Event image :::" + (null!=resultSet.getBytes(("Event_img"))));
				if(null!=resultSet.getBytes(("Event_img"))) {
					event.setEventString(Base64.getEncoder().encodeToString(resultSet.getBytes(("Event_img"))));
				}
				event.setEventName(resultSet.getString("Event_name"));
				event.setEventDate(resultSet.getString("Event_date"));
				event.setEventDesc(resultSet.getString("Description"));
				eventList.add(event);
				
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
		
		return eventList;
	}

	@Override
	public int updateEvent(Connection connection, Event event) {
		try(PreparedStatement preparedStatement = connection.prepareStatement("update eventdetails_table set Event_id=?, Event_img=?, Event_name=?,Event_date=?,Description=? where Event_id=? ")){
			preparedStatement.setInt(1,event.getEventId());
			preparedStatement.setBlob(2,event.getEventStream());
			preparedStatement.setString(3,event.getEventName());
			preparedStatement.setString(4,event.getEventDate());
			preparedStatement.setString(5,event.getEventDesc());
			preparedStatement.setInt(6,event.getEventId());
			return preparedStatement.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Event selectEventDetails(Connection connection, String EventId) {
		Event event = new Event();
		
		try(PreparedStatement preparedStatement = connection.prepareStatement("select * from eventdetails_table where Event_id= ?")){
			preparedStatement.setInt(1, Integer.parseInt(EventId));
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				System.out.println("Event image :::" + (null!=resultSet.getBytes(("Event_img"))));
				if(null!=resultSet.getBytes(("Event_img"))) {
					event.setEventString(Base64.getEncoder().encodeToString(resultSet.getBytes(("Event_img"))));
				}
				event.setEventName(resultSet.getString("Event_name"));
				event.setEventDate(resultSet.getString("Event_date"));
				event.setEventDesc(resultSet.getString("Description"));
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
		return event;
	}
}
