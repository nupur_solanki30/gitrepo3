package com.HostoPedia.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;
import com.HostoPedia.dao.ProfileDao;

public class ProfileDaoImpl implements ProfileDao{

	@Override
	public int updateUserDetails(Connection connection, User user) throws SQLException {
		System.out.println("Profile dao called");
		System.out.println("Update user method called");

		try(PreparedStatement preparedStatement = connection.prepareStatement("update user_table set User_name=?, User_email_id=?, User_contact=?, User_add=?, User_role=? where User_id=?")){
			
			preparedStatement.setString(1, user.getUserName());
			preparedStatement.setString(2, user.getUserEmailid());
			preparedStatement.setString(3, user.getUserContact());
			preparedStatement.setString(4, user.getUserAdd());
			preparedStatement.setString(5, user.getUserRole());
			preparedStatement.setInt(6, user.getUserId());
			
			System.out.println("Update user method completed");
			System.out.println("Profile dao completed");
			return preparedStatement.executeUpdate();
		}
	}

	@Override
	public int updateStudentDetails(Connection connection, Student student) throws SQLException {
		System.out.println("Profile dao called");
		System.out.println("Update student method called");
		String query = "update student_table set Stu_clgname =?,Stu_fathername = ?,stu_mothername= ? "
				+ ", stu_doc = COALESCE(?,Stu_Doc) "
				+ "where User_id = ?";
		
		int counter = 1;
		try(PreparedStatement ps = connection.prepareStatement(query)){
			
			ps.setString(counter++, student.getStuClgName());
			ps.setString(counter++, student.getStuFatherName());
			ps.setString(counter++, student.getStuMotherName());
			ps.setBlob(counter++, student.getStuDocStream());
          //ps.setString(counter++,student.getStuDob());
			ps.setInt(counter++, student.getUserId());

		 //ps.setString(7, student.getUserRole());
			System.out.println("Dao layer completed");
			System.out.println("update student method completed");
			return ps.executeUpdate();
		}
	}
}
