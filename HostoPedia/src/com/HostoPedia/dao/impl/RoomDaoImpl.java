package com.HostoPedia.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;

import com.HostoPedia.bean.Event;
import com.HostoPedia.bean.Room;
import com.HostoPedia.dao.RoomDao;

public class RoomDaoImpl implements RoomDao {

	@Override
	public int insertRoom(Connection connection, Room room) {
		// TODO Auto-generated method stub
				System.out.println("Room dao impl start");
				String insertQuery="insert into room_table(Room_id, Room_no, Floor_no, Room_img, Description, Room_capacity) values(?,?,?,?,?,?)";
				
				int i=0;
				int recordId=0;
				try (PreparedStatement preparedStatement=connection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS))
						{
							preparedStatement.setInt(1,room.getRoomId());
							preparedStatement.setInt(2,room.getRoomNo());
							preparedStatement.setInt(3,room.getFloorNo());
							preparedStatement.setBlob(4, room.getRoomStream());
							preparedStatement.setString(5,room.getRoomDesc());
							preparedStatement.setInt(6,room.getRoomCape());
							i = preparedStatement.executeUpdate();
							
							ResultSet resultSet = preparedStatement.getGeneratedKeys();
							while(resultSet.next()) {
									recordId = resultSet.getInt(1);
							}
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				System.out.println("room dao impl finish");
				return recordId;
				
	}

	@Override
	public int deleteRoom(Connection connection, String RoomId) {
		System.out.println("Dao layer called");
		System.out.println("Delete room method called");
		int roomid=Integer.parseInt(RoomId);
		String deleteRoomQuery= "delete from room_table where Room_id = ?";
		try(PreparedStatement preparedStatement = connection.prepareStatement(deleteRoomQuery)){
			preparedStatement.setInt(1, roomid);
			return preparedStatement.executeUpdate();
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		System.out.println("Delete room method completed");
		System.out.println("Dao layer completed");
		return 0;
	}

	@Override
	public ArrayList<Room> selectRoom(Connection connection) {
		ArrayList<Room> roomList = new ArrayList<>();
		try(PreparedStatement preparedStatement = connection.prepareStatement("select * from room_table");
				ResultSet resultSet = preparedStatement.executeQuery();){
			
			while(resultSet.next()) {
				Room room = new Room();
				
				room.setRoomId(resultSet.getInt("Room_id"));
				room.setRoomNo(resultSet.getInt("Room_no"));
				room.setFloorNo(resultSet.getInt("Floor_no"));
				System.out.println("Room image :::" + (null!=resultSet.getBytes(("Room_img"))));
				if(null!=resultSet.getBytes(("Room_img"))) {
					room.setRoomString(Base64.getEncoder().encodeToString(resultSet.getBytes(("Room_img"))));
				}
				room.setRoomDesc(resultSet.getString("Description"));
				room.setRoomCape(resultSet.getInt("Room_capacity"));
				roomList.add(room);		
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
		
		return roomList;
	}

	@Override
	public int updateRoom(Connection connection, Room room) {
		try(PreparedStatement preparedStatement = connection.prepareStatement("update room_table set Room_id=?, Room_no=?, Floor_no=?,Room_img=?,Description=?, Room_capacity=? where Room_id=? ")){
			preparedStatement.setInt(1,room.getRoomId());
			preparedStatement.setInt(2,room.getRoomNo());
			preparedStatement.setInt(3,room.getFloorNo());
			preparedStatement.setBlob(4,room.getRoomStream());
			preparedStatement.setString(5,room.getRoomDesc());
			preparedStatement.setInt(6,room.getRoomCape());
			preparedStatement.setInt(7,room.getRoomId());
			return preparedStatement.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Room selectRoomDetails(Connection connection, String RoomId) {
		Room room = new Room();
		
		try(PreparedStatement preparedStatement = connection.prepareStatement("select * from room_table  where Room_id= ?")){
			preparedStatement.setInt(1, Integer.parseInt(RoomId));
			ResultSet resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next()) {
				System.out.println("Room image :::" + (null!=resultSet.getBytes(("Room_img"))));
				if(null!=resultSet.getBytes(("Room_img"))) {
					room.setRoomString(Base64.getEncoder().encodeToString(resultSet.getBytes(("Room_img"))));
				}
				room.setRoomNo(resultSet.getInt("Room_no"));
				room.setFloorNo(resultSet.getInt("Floor_no"));
				room.setRoomDesc(resultSet.getString("Description"));
				room.setRoomCape(resultSet.getInt("Room_capacity"));
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
		return room;
	}

	@Override
	public int getRoomCountFromUserTable(Connection connection ,int roomId) {
		
		int roomCount = 0 ;
		try(PreparedStatement preparedStatement = connection.prepareStatement("select count(*) as roomCount from student_table  where Room_id= ?")){
			preparedStatement.setInt(1, roomId);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next()) {
				
				return resultSet.getInt("roomCount");
				
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
		
		return 0;
	}

}
