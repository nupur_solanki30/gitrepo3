package com.HostoPedia.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.HostoPedia.bean.Food;
import com.HostoPedia.bean.RulesRegulationsBean;
import com.HostoPedia.dao.FoodDao;

public class FoodDaoImpl implements FoodDao {

	@Override
	public int insertFood(Connection connection, Food food) {
		System.out.println("Food dao impl start");
		String insertQuery="insert into food_table(Food_name, Food_type, Day) values(?,?,?)";
		
		int i=0;
		int recordId=0;
		try (PreparedStatement preparedStatement=connection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS))
				{
					preparedStatement.setString(1,food.getFoodName());
					preparedStatement.setString(2,food.getFoodType());
					preparedStatement.setString(3,food.getDay());
					
					i = preparedStatement.executeUpdate();
					
					ResultSet resultSet = preparedStatement.getGeneratedKeys();
					while(resultSet.next()) {
							recordId = resultSet.getInt(1);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		System.out.println("Food dao impl finish");
		return recordId;
	}

	@Override
	public int deleteFood(Connection connection, String FoodId) {
		System.out.println("Dao layer called");
		System.out.println("Delete food method called");
		int foodid=Integer.parseInt(FoodId);
		String deletefoodQuery= "delete from food_table where Food_id = ?";
		try(PreparedStatement preparedStatement = connection.prepareStatement(deletefoodQuery)){
			preparedStatement.setInt(1, foodid);
			return preparedStatement.executeUpdate();
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		System.out.println("Delete food method completed");
		System.out.println("Dao layer completed");
		return 0;
	}

	@Override
	public ArrayList<Food> selectFood(Connection connection) {

		ArrayList<Food> foodList = new ArrayList<>();
		try(PreparedStatement preparedStatement = connection.prepareStatement("select * from food_table");
				ResultSet resultSet = preparedStatement.executeQuery();){
			
			while(resultSet.next()) {
				Food food =  new Food();
				food.setFoodId(resultSet.getInt("Food_id"));
				food.setFoodName(resultSet.getString("Food_name"));
				food.setFoodType(resultSet.getString("Food_type"));
				food.setDay(resultSet.getString("Day"));
				foodList.add(food);
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
		
		return foodList;
	}

	@Override
	public int updateFood(Connection connection, Food food) {
		try(PreparedStatement preparedStatement = connection.prepareStatement("update food_table  set Food_id=?, Food_name=?, Food_type=?, Day=?  where  Food_id=?")){
			
			preparedStatement.setInt(1,food.getFoodId());
			preparedStatement.setString(2,food.getFoodName());
			preparedStatement.setString(3,food.getFoodType());
			preparedStatement.setString(4,food.getDay());
			preparedStatement.setInt(5,food.getFoodId());
			
			return preparedStatement.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Food selectFood(Connection connection, String foodId) {
		Food food = new Food();		
		try(PreparedStatement preparedStatement = connection.prepareStatement("select * from food_table where Food_id = ?")){
			preparedStatement.setInt(1, Integer.parseInt(foodId));
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {

				food.setFoodName(resultSet.getString("Food_name"));
				food.setFoodType(resultSet.getString("Food_type"));
				food.setDay(resultSet.getString("Day"));			
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}		
		return food;
	}

}
