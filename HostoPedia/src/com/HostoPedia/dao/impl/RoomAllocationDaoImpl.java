package com.HostoPedia.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;

import com.HostoPedia.bean.Room;
import com.HostoPedia.dao.RoomAllocationDao;

public class RoomAllocationDaoImpl  implements RoomAllocationDao {

	public ArrayList<Room> selectRoom(Connection connection) {
		ArrayList<Room> roomList = new ArrayList<>();
		try(PreparedStatement preparedStatement = connection.prepareStatement("select * from student_table where Room_id=?");
				ResultSet resultSet = preparedStatement.executeQuery();){
			
			while(resultSet.next()) {
				Room room = new Room();
				
				room.setRoomId(resultSet.getInt("Room_id"));
				room.setRoomNo(resultSet.getInt("Room_no"));
				room.setFloorNo(resultSet.getInt("Floor_no"));
				System.out.println("Room image :::" + (null!=resultSet.getBytes(("Room_img"))));
				if(null!=resultSet.getBytes(("Room_img"))) {
					room.setRoomString(Base64.getEncoder().encodeToString(resultSet.getBytes(("Room_img"))));
				}
				room.setRoomDesc(resultSet.getString("Description"));
				room.setRoomCape(resultSet.getInt("Room_capacity"));
				roomList.add(room);		
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
		
		return roomList;
	}
}
