package com.HostoPedia.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;

import com.HostoPedia.bean.Gallery;
import com.HostoPedia.dao.GalleryDao;

public class GalleryDaoImpl implements GalleryDao {

	@Override
	public int insertGallery(Connection connection, Gallery gallery) {
		// TODO Auto-generated method stub
		
		System.out.println("Gallery dao impl start");
		String insertQuery="insert into gallary_table(gallary_id, Img_path) values(?,?)";
		
		int i=0;
		int recordId=0;
		try (PreparedStatement preparedStatement=connection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS))
				{
					preparedStatement.setInt(1, gallery.getGalleryId());
					preparedStatement.setBlob(2, gallery.getGalleryStream());
					
					i = preparedStatement.executeUpdate();
					
					ResultSet resultSet = preparedStatement.getGeneratedKeys();
					while(resultSet.next()) {
							recordId = resultSet.getInt(1);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		System.out.println("Gallery dao impl finish");
		return recordId;
	}

	@Override
	public int deleteGallery(Connection connection, String GalleryId) {
		// TODO Auto-generated method stub
		System.out.println("Dao layer called");
		System.out.println("Delete gallery method called");
		int galleryid=Integer.parseInt(GalleryId);
		String deleteEventQuery= "delete from gallary_table where gallary_id = ?";
		try(PreparedStatement preparedStatement = connection.prepareStatement(deleteEventQuery)){
			preparedStatement.setInt(1, galleryid);
			return preparedStatement.executeUpdate();
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		System.out.println("Delete gallery method completed");
		System.out.println("Dao layer completed");
		return 0;
	}

	@Override
	public ArrayList<Gallery> selectGallery(Connection connection) {
		// TODO Auto-generated method stub
		ArrayList<Gallery> galleryList = new ArrayList<>();
		try(PreparedStatement preparedStatement = connection.prepareStatement("select * from gallary_table");
				ResultSet resultSet = preparedStatement.executeQuery();){
			
			while(resultSet.next()) {
				Gallery gallery = new Gallery();
				
				gallery.setGalleryId(resultSet.getInt("gallary_id"));
				System.out.println("Gallery image :::" + (null!=resultSet.getBytes(("Img_path"))));
				if(null!=resultSet.getBytes(("Img_path"))) {
					gallery.setGalleryString(Base64.getEncoder().encodeToString(resultSet.getBytes(("Img_path"))));
				}
				galleryList.add(gallery);	
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
		return galleryList;
	}

	@Override
	public int updateGallery(Connection connection, Gallery gallery) {
		// TODO Auto-generated method stub
		try(PreparedStatement preparedStatement = connection.prepareStatement("update gallary_table set gallary_id=?, Img_path = COALESCE(?,Img_path) where gallary_id=? ")){
			preparedStatement.setInt(1,gallery.getGalleryId());
			preparedStatement.setBlob(2,gallery.getGalleryStream());
			preparedStatement.setInt(3,gallery.getGalleryId());
			return preparedStatement.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Gallery selectGalleryDetails(Connection connection, String GalleryId) {
		// TODO Auto-generated method stub
		Gallery gallery = new Gallery();
		
		try(PreparedStatement preparedStatement = connection.prepareStatement("select * from gallary_table where gallary_id = ?")){
			preparedStatement.setInt(1, Integer.parseInt(GalleryId));
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				System.out.println("Gallery image :::" + (null!=resultSet.getBytes(("Img_path"))));
				if(null!=resultSet.getBytes(("Img_path"))) {
					gallery.setGalleryString(Base64.getEncoder().encodeToString(resultSet.getBytes(("Img_path"))));
				}
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
		return gallery;
	}
}
