package com.HostoPedia.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;

import com.HostoPedia.bean.Facility;
import com.HostoPedia.bean.RulesRegulationsBean;
import com.HostoPedia.dao.FacilityDao;


public class FacilityDaoImpl implements FacilityDao {

	@Override
	public int insertFacility(Connection connection, Facility facility) {
		// TODO Auto-generated method stub
		System.out.println("facility dao impl start");
		String insertQuery="insert into facility_table(Facility_id, Facility_img, Facility_name, Description) values(?,?,?,?)";
		
		int i=0;
		int recordId=0;
		try (PreparedStatement preparedStatement=connection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS))
				{
					preparedStatement.setInt(1,facility.getFacilityId());
					preparedStatement.setBlob(2,facility.getFacilityStream());
					preparedStatement.setString(3,facility.getFacilityName());
					preparedStatement.setString(4,facility.getDescription());
					
					i = preparedStatement.executeUpdate();
					
					ResultSet resultSet = preparedStatement.getGeneratedKeys();
					while(resultSet.next()) {
							recordId = resultSet.getInt(1);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		System.out.println("facility dao impl finish");
		return recordId;
	}

	@Override
	public int deleteFacility(Connection connection, String FacilityId) {
		// TODO Auto-generated method stub
		System.out.println("Dao layer called");
		System.out.println("Delete facility method called");
		int facilityid=Integer.parseInt(FacilityId);
		String deleteFacilityQuery= "delete from facility_table where Facility_id = ?";
		try(PreparedStatement preparedStatement = connection.prepareStatement(deleteFacilityQuery)){
			preparedStatement.setInt(1, facilityid);
			return preparedStatement.executeUpdate();
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		System.out.println("Delete facility method completed");
		System.out.println("Dao layer completed");
		return 0;
	
	}

	@Override
	public ArrayList<Facility> selectFacility(Connection connection) {
		// TODO Auto-generated method stub
		ArrayList<Facility> facilityList = new ArrayList<>();
		try(PreparedStatement preparedStatement = connection.prepareStatement("select * from facility_table");
				ResultSet resultSet = preparedStatement.executeQuery();){
			
			while(resultSet.next()) {
				Facility facility = new Facility();
				
				facility.setFacilityId(resultSet.getInt("Facility_id"));
				System.out.println("Facility image :::" + (null!=resultSet.getBytes(("Facility_img"))));
				if(null!=resultSet.getBytes(("Facility_img"))) {
					facility.setFacilityString(Base64.getEncoder().encodeToString(resultSet.getBytes(("Facility_img"))));
				}

				facility.setDescription(resultSet.getString("Description"));
				facility.setFacilityName(resultSet.getString("Facility_name"));
				facilityList.add(facility);
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
		
		return facilityList;
	}


	@Override
	public int updateFacility(Connection connection, Facility facility) {
		// TODO Auto-generated method stub
		try(PreparedStatement preparedStatement = connection.prepareStatement("update facility_table set Facility_id=?, Facility_img=?, Facility_name=?, Description=? where Facility_id=? ")){
			preparedStatement.setInt(1,facility.getFacilityId());
			preparedStatement.setBlob(2,facility.getFacilityStream());
			preparedStatement.setString(3,facility.getFacilityName());
			preparedStatement.setString(4,facility.getDescription());
			preparedStatement.setInt(5,facility.getFacilityId());
			return preparedStatement.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Facility selectFacilityDetails(Connection connection, String facilityId) {
		// TODO Auto-generated method stub
		Facility facility = new Facility();
		
		try(PreparedStatement preparedStatement = connection.prepareStatement("select * from facility_table where Facility_id= ?")){
			preparedStatement.setInt(1, Integer.parseInt(facilityId));
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				
				System.out.println("Facility image :::" + (null!=resultSet.getBytes(("Facility_img"))));
				if(null!=resultSet.getBytes(("Facility_img"))) {
					facility.setFacilityString(Base64.getEncoder().encodeToString(resultSet.getBytes(("Facility_img"))));
				}
				facility.setFacilityName(resultSet.getString("Facility_name"));
				facility.setDescription(resultSet.getString("Description"));
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
	
		return facility;
	}
}


