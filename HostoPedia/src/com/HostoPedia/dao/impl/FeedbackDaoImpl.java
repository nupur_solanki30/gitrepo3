package com.HostoPedia.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.HostoPedia.bean.Feedback;
import com.HostoPedia.dao.FeedbackDao;

public class FeedbackDaoImpl implements FeedbackDao {

	@Override
	public int saveFeedbackDetails(Connection connection, Feedback feedBack) {
		// TODO Auto-generated method stub
		System.out.println("Save Feedback Detail start");
		String insertQuery = "insert into feedback_table(Feedback_des, Feedback_rating, Feedback_date, User_id) values(?,?,?,?)";
		try(PreparedStatement preparedStatement = connection.prepareStatement(insertQuery))
		{
			preparedStatement.setString(1, feedBack.getFeedbackDescription());
			preparedStatement.setString(2, feedBack.getFeedbackRating());
			preparedStatement.setString(3, feedBack.getFeedbackDate());
			preparedStatement.setInt(4, feedBack.getUserId());
			return preparedStatement.executeUpdate();
			
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Save Feedback Detail end");
		return 0;
	}

	@Override
	public List<Feedback> selectFeedbackDetails(Connection connection, int feedbackId) {
		String selectQuery="select ft.Feebback_id,ft.Feedback_des, ft.Feedback_rating,ft.Feedback_date,ut.User_name from feedback_table ft, user_table ut where ft.Feedback_id=? and ft.User_id=ut.User_id";
		List<Feedback> feedbackList=new ArrayList<Feedback>();
		try(PreparedStatement preparedStatement=connection.prepareStatement(selectQuery))
		{
			preparedStatement.setInt(1, feedbackId);
			
			try(ResultSet resultSet=preparedStatement.executeQuery())
			{
				while(resultSet.next())
				{
					int feedbackID=resultSet.getInt("Feedback_id");
					String description=resultSet.getString("Feedback_des");
					String rating=resultSet.getString("Feedback_rating");
					String date=resultSet.getString("Feedback_date");
					String userName=resultSet.getString("User_name");
					
					Feedback feedBack = new Feedback();
					feedBack.setFeedbackId(feedbackID);
					feedBack.setFeedbackDescription(description);
					feedBack.setFeedbackRating(rating);
					feedBack.setFeedbackDate(date);
					feedBack.setUserName(userName);
					
					feedbackList.add(feedBack);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return feedbackList;
	}

	@Override
	public List<Feedback> selectFeedbackDetails(Connection connection) {
		String selectQuery="select ft.Feedback_id,ft.Feedback_des, ft.Feedback_rating,ft.Feedback_date,ut.User_name from feedback_table ft, user_table ut where ft.User_id=ut.User_id";
		List<Feedback> feedbackList=new ArrayList<Feedback>();
		try(PreparedStatement preparedStatement=connection.prepareStatement(selectQuery))
		{
			try(ResultSet resultSet=preparedStatement.executeQuery())
			{
				while(resultSet.next())
				{
					int feedbackId=resultSet.getInt("Feedback_id");
					String description=resultSet.getString("Feedback_des");
					String rating=resultSet.getString("Feedback_rating");
					String date=resultSet.getString("Feedback_date");
					String userName=resultSet.getString("User_name");
					
					Feedback feedBack=new Feedback();
					feedBack.setFeedbackId(feedbackId);
					feedBack.setFeedbackDescription(description);
					feedBack.setFeedbackRating(rating);
					feedBack.setFeedbackDate(date);
					feedBack.setUserName(userName);
					feedbackList.add(feedBack);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return feedbackList;
	}

	@Override
	public int deleteFeedbackDetails(Connection connection, String FeedbackId) {
		// TODO Auto-generated method stub
		String deleteQuery="delete from feedback_table where Feedback_id=?";
		int feedbackid=Integer.parseInt(FeedbackId);
		try(PreparedStatement preparedStatement=connection.prepareStatement(deleteQuery))
		{
			preparedStatement.setInt(1, feedbackid);
	
			return preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
}
