package com.HostoPedia.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.HostoPedia.bean.Booking;
import com.HostoPedia.dao.BookingDao;

public class BookinDaoImpl implements BookingDao {

	@Override
	public int saveBookingDetails(Connection connection, int roomId, int stuId, double price) {
		String insertQuery="insert into bookhostel_table(Payment_id, Stu_id, Booking_date, Booking_status, Payment_status, Booking_dur, Amount, Room_id) values(?,?,?,?,?,?,?,?)";
		try(PreparedStatement preparedStatement=connection.prepareStatement(insertQuery))
		{
			preparedStatement.setString(1,null);
			preparedStatement.setInt(2, stuId);
			preparedStatement.setString(3, null);
			preparedStatement.setInt(4, 1);
			preparedStatement.setInt(5, 0);
			preparedStatement.setString(6, null);
			preparedStatement.setDouble(7,price);
			preparedStatement.setInt(8, roomId);
			return preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public List<Booking> selectBookingDetails(Connection connection) {
		String selectQuery="select * from bookhostel_table";
		List<Booking> bookingList=new ArrayList<Booking>();
		
		try(PreparedStatement preparedStatement=connection.prepareStatement(selectQuery);
				ResultSet resultSet=preparedStatement.executeQuery())
		{
			while(resultSet.next())
			{
				Booking booking=new Booking();
				Integer bookingId=resultSet.getInt("Booking_id");
				Integer stuId=resultSet.getInt("Stu_id");
				String date=resultSet.getString("Booking_date");
				Integer bookingStatus=resultSet.getInt("Booking_status");
				Integer paymentStatus=resultSet.getInt("Payment_status");
				Integer duration=resultSet.getInt("Booking_dur");
				Integer ammount=resultSet.getInt("I_ammount");
				Integer roomId=resultSet.getInt("Room_id");
				
				booking.setBookingId(bookingId);
				booking.setStuId(stuId);
				booking.setBookingDate(date);
				booking.setBookingStatus(bookingStatus);
				booking.setPaymentStatus(paymentStatus);
				booking.setBookingDuration(duration);
				booking.setAmmount(ammount);
				booking.setRoomId(roomId);
				
				bookingList.add(booking);
				
		
				//userList.add(user);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return bookingList;
	}

	@Override
	public int editBookingDetails(Connection connection, Booking booking) {
		System.out.println("Edit Booking Details called");
		String updateQuery="update bookhostel_table set Payment_id=?,Booking_date=?,Payment_status=? where Booking_status=? ";
		try(PreparedStatement preparedStatement=connection.prepareStatement(updateQuery))
		{
			preparedStatement.setString(1,booking.getPaymentId());
			preparedStatement.setString(2,booking.getBookingDate());
			preparedStatement.setInt(3, booking.getPaymentStatus());
			preparedStatement.setInt(4,1);
			
			System.out.println("Edit Booking Details end");
			return preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int saveBookingDetails(Connection connection, Booking booking) {
		System.out.println("Save Booking Details called");
		String insertQuery="insert into bookhostel_table(Payment_id, Stu_id, Room_id, Booking_status, Amount, Payment_status) values(?,?,?,?,?,?)";
		try(PreparedStatement preparedStatement=connection.prepareStatement(insertQuery))
		{
			preparedStatement.setString(1, booking.getPaymentId());
			preparedStatement.setInt(2, booking.getStuId());
			preparedStatement.setInt(3, booking.getRoomId());
			preparedStatement.setInt(4, 1);
			preparedStatement.setDouble(5, booking.getAmmount());
			preparedStatement.setInt(6, 0);
			System.out.println("Save Booking Details end");
			return preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
}
