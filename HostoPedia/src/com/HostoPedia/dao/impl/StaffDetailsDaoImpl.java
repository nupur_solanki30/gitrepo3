package com.HostoPedia.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.HostoPedia.bean.RulesRegulationsBean;
import com.HostoPedia.bean.StaffDetails;
import com.HostoPedia.dao.StaffDetailsDao;

public class StaffDetailsDaoImpl implements StaffDetailsDao {

	@Override
	public int insertStafftDetails(Connection connection, StaffDetails staffDetails) {
		// TODO Auto-generated method stub
		System.out.println("Staff details dao impl start");
		String insertQuery="insert into staffdetails_table(Staff_name, Staff_contact, Staff_add, Staff_gender, Staff_designation) values(?,?,?,?,?)";
		
		int i=0;
		int recordId=0;
		try (PreparedStatement preparedStatement=connection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS))
				{
					preparedStatement.setString(1,staffDetails.getStaffName());
					preparedStatement.setString(2,staffDetails.getStaffContact());
					preparedStatement.setString(3,staffDetails.getStaffAdd());
					preparedStatement.setString(4,staffDetails.getGender());
					preparedStatement.setString(5,staffDetails.getDesignation());
					
					i = preparedStatement.executeUpdate();
					
					ResultSet resultSet = preparedStatement.getGeneratedKeys();
					while(resultSet.next()) {
							recordId = resultSet.getInt(1);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		System.out.println("Staff details dao impl finish");
		return recordId;
	}

	@Override
	public int deleteStaffDetails(Connection connection, String StaffId) {
		// TODO Auto-generated method stub
		System.out.println("Dao layer called");
		System.out.println("Delete staffdetais method called");
		int staffid=Integer.parseInt(StaffId);
		String deleteStaffDetailsQuery= "delete from staffdetails_table where  Staff_id = ?";
		try(PreparedStatement preparedStatement = connection.prepareStatement(deleteStaffDetailsQuery)){
			preparedStatement.setInt(1, staffid);
			return preparedStatement.executeUpdate();
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		System.out.println("Delete staffdetails method completed");
		System.out.println("Dao layer completed");
		return 0;
	}

	@Override
	public ArrayList<StaffDetails> selectStaffDetails(Connection connection) {
		// TODO Auto-generated method stub
		ArrayList<StaffDetails> staffDetailsList = new ArrayList<>();
		try(PreparedStatement preparedStatement = connection.prepareStatement("select * from staffdetails_table");
				ResultSet resultSet = preparedStatement.executeQuery();){
			
			while(resultSet.next()) {
				StaffDetails staffDetails = new StaffDetails();
				
				staffDetails.setStaffId(resultSet.getInt("Staff_id"));
				staffDetails.setStaffName(resultSet.getString("Staff_name"));
				staffDetails.setStaffContact(resultSet.getString("Staff_contact"));
				staffDetails.setStaffAdd(resultSet.getString("Staff_add"));
				staffDetails.setGender(resultSet.getString("Staff_gender"));
				staffDetails.setDesignation(resultSet.getString("Staff_designation"));
				staffDetailsList.add(staffDetails);
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
		
		return staffDetailsList;
	}

	@Override
	public int updateStaffDetails(Connection connection, StaffDetails staffDetails) {
		// TODO Auto-generated method stub
try(PreparedStatement preparedStatement = connection.prepareStatement("update staffdetails_table set Staff_id=?, Staff_name=?, Staff_contact=?, Staff_add=?, Staff_gender=?, Staff_designation=? where Staff_id=?")){
			
			preparedStatement.setInt(1,staffDetails.getStaffId());
			preparedStatement.setString(2,staffDetails.getStaffName());
			preparedStatement.setString(3,staffDetails.getStaffContact());
			preparedStatement.setString(4,staffDetails.getStaffAdd());
			preparedStatement.setString(5,staffDetails.getGender());
			preparedStatement.setString(6,staffDetails.getDesignation());
			preparedStatement.setInt(7,staffDetails.getStaffId());
			return preparedStatement.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public StaffDetails selectStaffDetails(Connection connection, String staffId) {
		// TODO Auto-generated method stub
		StaffDetails staffDetails = new StaffDetails();
		
		try(PreparedStatement preparedStatement = connection.prepareStatement("select * from staffdetails_table where Staff_id = ?")){
			preparedStatement.setInt(1, Integer.parseInt(staffId));
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				
				staffDetails.setStaffName(resultSet.getString("Staff_name"));
				staffDetails.setStaffContact(resultSet.getString("Staff_contact"));
				staffDetails.setStaffAdd(resultSet.getString("Staff_add"));
				staffDetails.setGender(resultSet.getString("Staff_gender"));
				staffDetails.setDesignation(resultSet.getString("Staff_designation"));
			}
		}catch (Exception e) {
			e.printStackTrace();		
		}
		
		return staffDetails;
	}
}
