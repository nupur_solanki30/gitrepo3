package com.HostoPedia.service;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;

public interface ProfileService {
	public String modifyUserDetails(User user, Student student);
}
