package com.HostoPedia.service;

import java.util.ArrayList;
import com.HostoPedia.bean.Event;

public interface EventService {
	public String removeEvent(String eventId);

	public ArrayList<Event> fetchEvent();

	public String modifyEvent(Event event);
	
	public String saveEventDetails(Event event);
	
	public Event fetchEventDetails(String eventId);
}
