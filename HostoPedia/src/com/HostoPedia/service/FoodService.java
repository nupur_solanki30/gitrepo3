package com.HostoPedia.service;

import java.util.ArrayList;

import com.HostoPedia.bean.Food;

public interface FoodService {
	public String removeFood(String foodId);

	public ArrayList<Food> fetchFood();

	public String modifyFood(Food food);
	
	public String saveFood(Food food);
	
	public Food fetchFood(String foodId);
}
