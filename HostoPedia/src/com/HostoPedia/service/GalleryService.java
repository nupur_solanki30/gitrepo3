package com.HostoPedia.service;

import java.util.ArrayList;
import com.HostoPedia.bean.Gallery;

public interface GalleryService {
	public String removeGallery(String galleryId);

	public ArrayList<Gallery> fetchGallery();

	public String modifyGallery(Gallery gallery);
	
	public String saveGalleryDetails(Gallery gallery);
	
	public Gallery fetchGalleryDetails(String galleryId);
}
