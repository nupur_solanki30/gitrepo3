package com.HostoPedia.service;

import java.util.ArrayList;
import java.util.List;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;


public interface UserService {

	char[] ValidateUser(String email);

	public String saveUserDetails(User user);
	
	public List<User> fetchUserDetails();

	public List<Student> fetchStudentDetails();
	
	public List<Student>  fetchBookingDetails();
	
	public String removeUserDetails(String userId);
	
	public String removeStudentDetails(String userId);

	public User fetchUserDetails(String userId,String userRole);

	public String modifyUserDetails(User user);
	
	public User fetchStudentDetails(String userId);
	
	public String modifyStudentDetails(User user, Student student);
	
	public ArrayList<User> fetchAllUserDetails();
	
	String modifyPassword(String password, String email);
}
