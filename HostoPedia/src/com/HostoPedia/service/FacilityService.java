package com.HostoPedia.service;

import java.util.ArrayList;

import com.HostoPedia.bean.Facility;

//import com.HostoPedia.bean.RulesRegulationsBean;

public interface FacilityService {

	public String removeFacility(String facilityId);

	public ArrayList<Facility> fetchFacility();

	public String modifyFacility(Facility facility);
	
	public String saveFacilityDetails(Facility facility);
	
	public Facility fetchFacilityDetails(String facilityId);
}
