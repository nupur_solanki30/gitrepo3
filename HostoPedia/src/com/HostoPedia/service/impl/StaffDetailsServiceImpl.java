package com.HostoPedia.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import com.HostoPedia.bean.StaffDetails;
import com.HostoPedia.dao.StaffDetailsDao;
import com.HostoPedia.dao.impl.StaffDetailsDaoImpl;
import com.HostoPedia.service.StaffDetailsService;

public class StaffDetailsServiceImpl  implements StaffDetailsService{
	
	StaffDetailsDao staffDetailsDao=new StaffDetailsDaoImpl();
	
public static Connection getConnection() {
		
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/hostopedia", "root", "root");	
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return connection;		
	}


	@Override
	public String removeStaff(String staffId) {
		// TODO Auto-generated method stub
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Service layer called");
		System.out.println("Remove staff method called");
		
		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";

		String message = null;

		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername,
				connectionPassword)) {

			int StaffId = Integer.parseInt(staffId);
			int deleteRecordId = staffDetailsDao.deleteStaffDetails(connection, staffId);
			if (deleteRecordId > 0) {
				message = "Staff deleted Successfully";
			} else {
				message = "Staff delete failed";
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
		System.out.println("Remove staff method completed");
		System.out.println("Service layer completed");
		
		return message;
	}

	@Override
	public ArrayList<StaffDetails> fetchStaff() {
		// TODO Auto-generated method stub
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}

		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";
		
		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername, connectionPassword)) {
			return  staffDetailsDao.selectStaffDetails(connection);
		   // return user;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	@Override
	public String modifyStaff(StaffDetails staffDetails) {
		// TODO Auto-generated method stub
		String message = null;
		try(Connection connection = getConnection()){
			int updatedRecordCount = staffDetailsDao.updateStaffDetails(connection, staffDetails);
			//(connection,rulesRegulationsBean);
			if(updatedRecordCount>0) {
				message = "Staff updated successfully";
			}else {
				message = "Staff not updated successfully";
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return message;
	}

	@Override
	public String saveStaffDetails(StaffDetails staffDetails) {
		// TODO Auto-generated method stub
		System.out.println("Service layer called");
		try(Connection connection=getConnection())
		{
			int recordID=staffDetailsDao.insertStafftDetails(connection, staffDetails);
		
			if(recordID > 0)
				return "Insert Staff Successfully";
			else
				return "Staff not Inserted";
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		System.out.println("Service layer finish");
		return null;
	}

	@Override
	public StaffDetails fetchStaffDetails(String staffId) {
		// TODO Auto-generated method stub
		try (Connection connection = getConnection()) {
			return staffDetailsDao.selectStaffDetails(connection, staffId);
	} catch (Exception exception) {
		exception.printStackTrace();
	}
		return null;
	}
}
