package com.HostoPedia.service.impl;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;
import com.HostoPedia.dao.RegistrationDao;
import com.HostoPedia.dao.impl.RegistrationDaoImpl;
import com.HostoPedia.service.RegistrationService;

public class RegistrationServiceImpl implements RegistrationService {
	RegistrationDao registrationDao=new RegistrationDaoImpl();

	public static Connection getConnection() {
	
	Connection connection = null;
	try {
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/hostopedia", "root", "root");	
	}catch(ClassNotFoundException e) {
		e.printStackTrace();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return connection;		
}

	@Override
	public String saveUserDetails(User user) {
		// TODO Auto-generated method stub
		System.out.println("Service layer called");
		String registrationMessage = null;
		
		try(Connection connection = getConnection()) {
			
			if(null != connection) {
				int insertedUserCount = registrationDao.insertUserDetails(user);
				System.out.println("InsertedUserCount "+insertedUserCount);
				if(insertedUserCount>0) 
					registrationMessage = "User Registation Successfull";
				else 
					registrationMessage = "User Registation failed";
			}else {
				System.out.println("Connection failed!");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Service layer completed");
		return registrationMessage;
	}

	@Override
	public String saveStudentDetails(User user, Student student) {
		// TODO Auto-generated method stub
		System.out.println("Service layer called");
		String registrationMessage = null;
		
		try(Connection connection = getConnection()) {
			
			if(null != connection) {
				int insertedUserCount = registrationDao.insertUserDetails(user);
				
				student.setUserId(insertedUserCount);
				int insertedRow = registrationDao.insertStudentDetails(student);
				
				System.out.println("InsertedUserCount "+insertedUserCount);
				if(insertedUserCount>0) 
					registrationMessage = "User Registation Successfull";
				else 
					registrationMessage = "User Registation failed";
			}else {
				System.out.println("Connection failed!");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Service layer completed");
		return registrationMessage;
	}
}


