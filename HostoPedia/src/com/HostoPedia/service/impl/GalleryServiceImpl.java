package com.HostoPedia.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import com.HostoPedia.bean.Gallery;
import com.HostoPedia.dao.GalleryDao;
import com.HostoPedia.dao.impl.GalleryDaoImpl;
import com.HostoPedia.service.GalleryService;

public class GalleryServiceImpl implements GalleryService {
	GalleryDao galleryDao = new GalleryDaoImpl();
	
	public static Connection getConnection() {
		
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/hostopedia", "root", "root");	
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return connection;		
	}
	
	@Override
	public String removeGallery(String galleryId) {
		// TODO Auto-generated method stub
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Service layer called");
		System.out.println("Remove gallery method called");
		
		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";

		String message = null;

		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername,
				connectionPassword)) {

			int GalleryId = Integer.parseInt(galleryId);
			int deleteRecordId = galleryDao.deleteGallery(connection, galleryId);
			if (deleteRecordId > 0) {
				message = "Gallery deleted Successfully";
			} else {
				message = "Gallery delete failed";
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
		System.out.println("Remove gallery method completed");
		System.out.println("Service layer completed");
		
		return message;
	}

	@Override
	public ArrayList<Gallery> fetchGallery() {
		// TODO Auto-generated method stub
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}

		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";
		
		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername, connectionPassword)) {
			return  galleryDao.selectGallery(connection);
		   // return user;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	@Override
	public String modifyGallery(Gallery gallery) {
		// TODO Auto-generated method stub
		String message = null;
		try(Connection connection = getConnection()){
			int updatedRecordCount = galleryDao.updateGallery(connection,gallery);
			if(updatedRecordCount>0) {
				message = "Gallery updated successfully";
			}else {
				message = "Gallery not updated successfully";
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return message;
	}

	@Override
	public String saveGalleryDetails(Gallery gallery) {
		// TODO Auto-generated method stub
		System.out.println("Service layer called");
		try(Connection connection=getConnection())
		{
			int recordID = galleryDao.insertGallery(connection, gallery);
		
			if(recordID > 0)
				return "Insert gallery Successfully";
			else
				return "Gallery not Inserted";
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		System.out.println("Service layer finish");
		return null;
	}

	@Override
	public Gallery fetchGalleryDetails(String galleryId) {
		// TODO Auto-generated method stub
		try (Connection connection = getConnection()) {
			return galleryDao.selectGalleryDetails(connection, galleryId);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}
}
