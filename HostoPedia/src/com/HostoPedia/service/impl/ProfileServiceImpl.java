package com.HostoPedia.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;
import com.HostoPedia.service.ProfileService;
import com.HostoPedia.dao.ProfileDao;
import com.HostoPedia.dao.impl.ProfileDaoImpl;

public class ProfileServiceImpl implements ProfileService {
	
	ProfileDao profileDao = new ProfileDaoImpl();

	public static Connection getConnection() {
	
	Connection connection = null;
	try {
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/hostopedia", "root", "root");	
	}catch(ClassNotFoundException e) {
		e.printStackTrace();
	} catch (SQLException e) {
		e.printStackTrace();
	}
		return connection;		
	}

	@Override
	public String modifyUserDetails(User user, Student student) {
		System.out.println("Profile service layer called");
		String msg = null;
		try(Connection connection = getConnection()){
			int updateUserDetails = profileDao.updateUserDetails(connection, student);
			if(updateUserDetails>0)
			{
				//student.setUserId(updatedRecordCount);
				int updatedRecordCount = profileDao.updateStudentDetails(connection, student);
			
			if(updatedRecordCount>0) {
				msg = "User updated successfully";
			}else {
				msg = "User updated failed";
			}
			}
		}catch (Exception e) {
			msg = e.getMessage();
			e.printStackTrace();
		}
		System.out.println("Profile service layer completed");
		return msg;
	}
}
