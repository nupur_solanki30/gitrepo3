package com.HostoPedia.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import com.HostoPedia.bean.Food;
import com.HostoPedia.dao.FoodDao;
import com.HostoPedia.dao.impl.FoodDaoImpl;
import com.HostoPedia.service.FoodService;

public class FoodServiceImpl implements FoodService{

	FoodDao foodDao = new FoodDaoImpl();
public static Connection getConnection() {
		
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/hostopedia", "root", "root");	
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return connection;		
	}

	@Override
	public String saveFood(Food food) {
		System.out.println("Service layer called");
		try(Connection connection=getConnection())
		{
			int recordID=foodDao.insertFood(connection, food);
		
			if(recordID > 0)
				return "Insert food Successfully";
			else
				return "food not Inserted";
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		System.out.println("Service layer finish");
		return null;
	}
	@Override
	public String removeFood(String foodId) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Service layer called");
		System.out.println("Remove food method called");
		
		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";

		String message = null;

		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername,
				connectionPassword)) {

			int FoodId = Integer.parseInt(foodId);
			int deleteRecordId = foodDao.deleteFood(connection, foodId);
			if (deleteRecordId > 0) {
				message = "Food deleted Successfully";
			} else {
				message = "Food delete failed";
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
		System.out.println("Remove food method completed");
		System.out.println("Service layer completed");
		
		return message;
	}

	@Override
	public ArrayList<Food> fetchFood() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}

		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";
		
		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername, connectionPassword)) {
			return  foodDao.selectFood(connection);
		   // return user;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	@Override
	public String modifyFood(Food food) {
		String message = null;
		try(Connection connection = getConnection()){
			int updatedRecordCount = foodDao.updateFood(connection,food);
			if(updatedRecordCount>0) {
				message = "food updated successfully";
			}else {
				message = "Food not updated successfully";
			}
		}catch (Exception e) {
			e.printStackTrace();
		}	
		return message;
	}
	
	@Override
	public Food fetchFood(String foodId) {
		try (Connection connection = getConnection()) {
			return foodDao.selectFood(connection, foodId);
	} catch (Exception exception) {
		exception.printStackTrace();
	}	
	return null;
	}
	
}
