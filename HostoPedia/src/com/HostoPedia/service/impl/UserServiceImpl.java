package com.HostoPedia.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;
import com.HostoPedia.dao.UserDao;
import com.HostoPedia.dao.impl.UserDaoImpl;
import com.HostoPedia.service.UserService;
//import com.hostelManagementSystem.service.UserService;



public class UserServiceImpl  implements UserService{

	UserDao userDao = new UserDaoImpl();
public static Connection getConnection() {
		
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/hostopedia", "root", "root");	
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return connection;		
	}

	@Override
	public char[] ValidateUser(String email) {
		// TODO Auto-generated method stub
		System.out.println("validate user service layer called");
		int selectedRow;
		int len=5;
		String str = "Invalid";
		char []msg=str.toCharArray();
		try(Connection connection=getConnection()){
			System.out.println("validate user service layer inside try");
			selectedRow= userDao.getUser(connection, email);
			System.out.println("selectedrow"+selectedRow);
			
			if(selectedRow>0)
			{
				String numbers = "0123456789";
				//Using random method
				Random rndm_method = new Random();
				char[] otp = new char[len];
				for(int i = 0; i< len; i++)
				{
					//use of charAt Method : to get character value
					//use of nextInt() as it is scanning the value as int
					otp[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
				}
				return otp;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("validate user service layer end");
		return msg;
	}
	@Override
	public String saveUserDetails(User user) {
		// TODO Auto-generated method stub
		try(Connection connection=getConnection())
		{
			int recordID=userDao.insertUserDetails(user);
		
			if(recordID > 0)
				return "Employee registration Successfull";
			else
				return "Employee registration failed";
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<User> fetchUserDetails() {
		// TODO Auto-generated method stub
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}

		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";
		
		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername,
				connectionPassword)) {
			return userDao.selectUserDetails(connection);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Student> fetchStudentDetails() {
		// TODO Auto-generated method stub
		System.out.println("service layer called");
		System.out.println("fetch student method called");
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}

		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";
		
		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername,
				connectionPassword)) {
			//return userDao.selectStudentDetails(connection);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		System.out.println("service layer completed");
		System.out.println("fetch student method completed");
		
		return null;
	}

	@Override
	public String removeUserDetails(String userId) {
		// TODO Auto-generated method stub
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";

		String message = null;

		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername,
				connectionPassword)) {

			Integer stdId = Integer.parseInt(userId);
			int deleteRecordId = userDao.deleteUserDetails(connection, userId);
			if (deleteRecordId > 0) {
				message = "user deleted Successfully";
			} else {
				message = "user delete failed";
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return message;
	}

	@Override
	public String removeStudentDetails(String userId) {
		// TODO Auto-generated method stub
		System.out.println("service layer called");
		System.out.println("remove student method called");
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";

		String message = null;
		Student student = new Student();
		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername,
				connectionPassword)) {

			Integer stdId = Integer.parseInt(userId);
			int deleteRecordId = userDao.deleteUserDetails(connection, userId);
			student.setUserId(deleteRecordId);
			int deletedRow = userDao.deleteStudentDetails(connection, userId);
			if (deleteRecordId > 0) {
				message = "user deleted Successfully";
			} else {
				message = "user delete failed";
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		System.out.println("service layer called");
		System.out.println("remove student method called");
		
		return message;
	}


	@Override
	public User fetchUserDetails(String userId, String userRole) {
		// TODO Auto-generated method stub
try (Connection connection = getConnection()) {
			
			if(userRole.equalsIgnoreCase("student")){
				return userDao.selectStudentDetails(connection, userId);
			}else {
				return  userDao.selectUserDetails(connection, userId);
			}// return user;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}


	@Override
	public String modifyUserDetails(User user) {
		// TODO Auto-generated method stub
		String message = null;
		try(Connection connection = getConnection()){
			int updatedRecordCount = userDao.updateUserDetails(connection,user);
			if(updatedRecordCount>0) {
				message = "user updated successfully";
			}else {
				message = "user not updated successfully";
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return message;
	}

	@Override
	public User fetchStudentDetails(String userId) {
		// TODO Auto-generated method stub
		System.out.println("service layer called");
		System.out.println("fetch student method called");
		
		try (Connection connection = getConnection()) {
			return  userDao.selectStudentDetails(connection, userId);
		   // return user;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		System.out.println("service layer completed");
		System.out.println("fetch student method completed");
		
		return null;
	}
	@Override
	public String modifyStudentDetails(User user, Student student) {
		// TODO Auto-generated method stub
		
		System.out.println("service layer called");
		System.out.println("modify student method called");
		String message = null;
		
		try(Connection connection = getConnection()){
			int updateUserDetails = userDao.updateUserDetails(connection, student);
			if(updateUserDetails>0) {
				
				int updatedRecordCount = userDao.updateStudentDetails(connection, student);
				
				if(updatedRecordCount>0) {
					message = " updated successfully";
				}else {
					message = " not updated successfully";
				}
			}
			
		}catch (Exception e) {
			message = e.getMessage();
			e.printStackTrace();
		}
		System.out.println("service layer completed");
		System.out.println("modify student method completed");
		return message;

	}
	@Override
	public ArrayList<User> fetchAllUserDetails() {
		// TODO Auto-generated method stub
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}

		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";
		
		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername,
				connectionPassword)) {
			return userDao.selectAllUserDetails(connection);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	@Override
	public String modifyPassword(String password, String email) {
		// TODO Auto-generated method stub
		
		String msg=null;
		try(Connection connection=getConnection())
		{
			int updateCount=userDao.updatePassword(connection,password,email);
			if(updateCount>0)
			{
				msg= "Password updated successfully";
				System.out.println("Password updated successfully");
			}
			else
			{
				msg= "Password updated failed";
				System.out.println("Password updated failed");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return msg;
	}

	@Override
	public List<Student> fetchBookingDetails() {
		// TODO Auto-generated method stub
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}

		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";
		
		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername,
				connectionPassword)) {
			return userDao.selectBookingDetails(connection);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}
}
		
