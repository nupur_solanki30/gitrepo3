package com.HostoPedia.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import com.HostoPedia.bean.RulesRegulationsBean;
import com.HostoPedia.dao.RulesRegulationsDao;
import com.HostoPedia.dao.impl.RulesRegulationsDaoImpl;
import com.HostoPedia.service.RulesRegulationsService;

public class RulesRegulationsServiceImpl implements RulesRegulationsService{
	RulesRegulationsDao rulesRegulationsDao = new RulesRegulationsDaoImpl();
public static Connection getConnection() {
		
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/hostopedia", "root", "root");	
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return connection;		
	}
public String saveRulesDetails(RulesRegulationsBean rulesRegulationsBean) {
	// TODO Auto-generated method stub
	System.out.println("Service layer called");
	try(Connection connection=getConnection())
	{
		int recordID=rulesRegulationsDao.insertRules(connection, rulesRegulationsBean);
	
		if(recordID > 0)
			return "Insert Rules Successfully";
		else
			return "Rules not Inserted";
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	System.out.println("Service layer finish");
	return null;
	
}
@Override
public String removeRules(String rulesId) {
	// TODO Auto-generated method stub
	try {
		Class.forName("com.mysql.cj.jdbc.Driver");
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	System.out.println("Service layer called");
	System.out.println("Remove rules method called");
	
	String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
	String connectionUsername = "root";
	String connectionPassword = "root";

	String message = null;

	try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername,
			connectionPassword)) {

		int RulesId = Integer.parseInt(rulesId);
		int deleteRecordId = rulesRegulationsDao.deleteRules(connection, rulesId);
		if (deleteRecordId > 0) {
			message = "Rule deleted Successfully";
		} else {
			message = "Rule delete failed";
		}

	} catch (Exception exception) {
		exception.printStackTrace();
	}
	
	System.out.println("Remove rules method completed");
	System.out.println("Service layer completed");
	
	return message;
}
@Override
public ArrayList<RulesRegulationsBean> fetchRules() {
	// TODO Auto-generated method stub
	try {
		Class.forName("com.mysql.cj.jdbc.Driver");
	} catch (ClassNotFoundException e) {
		
		e.printStackTrace();
	}

	String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
	String connectionUsername = "root";
	String connectionPassword = "root";
	
	try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername, connectionPassword)) {
		return  rulesRegulationsDao.selectRules(connection);
	   // return user;
	} catch (Exception exception) {
		exception.printStackTrace();
	}
	return null;
}

@Override
public String modifyRules(RulesRegulationsBean rulesRegulationsBean) {
	// TODO Auto-generated method stub
	String message = null;
	try(Connection connection = getConnection()){
		int updatedRecordCount = rulesRegulationsDao.updateRules(connection,rulesRegulationsBean);
		if(updatedRecordCount>0) {
			message = "Rules updated successfully";
		}else {
			message = "Rules not updated successfully";
		}
	}catch (Exception e) {
		e.printStackTrace();
	}
	
	return message;
}
@Override
public RulesRegulationsBean fetchRulesDetails(String rulesId) {
	
	try (Connection connection = getConnection()) {
			return rulesRegulationsDao.selectRulesDetails(connection, rulesId);
	} catch (Exception exception) {
		exception.printStackTrace();
	}
	
	return null;
}


}
