package com.HostoPedia.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.HostoPedia.bean.Room;
import com.HostoPedia.dao.RoomDao;
import com.HostoPedia.dao.impl.RoomDaoImpl;
import com.HostoPedia.service.RoomService;

public class RoomServiceImpl implements RoomService{
	RoomDao roomDao=new RoomDaoImpl();
	
public static Connection getConnection() {
		
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/hostopedia", "root", "root");	
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return connection;		
	}
	@Override
	public String removeRoom(String roomId) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Service layer called");
		System.out.println("Remove Room method called");
		
		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";

		String message = null;

		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername,
				connectionPassword)) {

			int RoomId = Integer.parseInt(roomId);
			int deleteRecordId = roomDao.deleteRoom(connection, roomId);
			if (deleteRecordId > 0) {
				message = "Room deleted Successfully";
			} else {
				message = "Room delete failed";
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
		System.out.println("Remove room method completed");
		System.out.println("Service layer completed");
		
		return message;
	}

	@Override
	public ArrayList<Room> fetchRoom() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}

		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";
		ArrayList<Room> rooms = null;
		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername, connectionPassword)) {
			rooms=   roomDao.selectRoom(connection);
			
			for(Room room : rooms ) {
				
				int roomCount = roomDao.getRoomCountFromUserTable(connection,room.getRoomId());
				if(roomCount == room.getRoomCape()) {
					room.setRoomAvailabilityStatus("Not Available");
				}else {
					room.setRoomAvailabilityStatus("Availalbe");
				}
			}
			
		   // return user;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return rooms;
	}

	@Override
	public String modifyRoom(Room room) {
		String message = null;
		try(Connection connection = getConnection()){
			int updatedRecordCount = roomDao.updateRoom(connection,room);
			if(updatedRecordCount>0) {
				message = "Room updated successfully";
			}else {
				message = "Room not updated successfully";
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return message;
	}

	@Override
	public String saveRoomDetails(Room room) {
		System.out.println("Service layer called");
		try(Connection connection=getConnection())
		{
			int recordID=roomDao.insertRoom(connection, room);
		
			if(recordID > 0)
				return "Insert Room Successfully";
			else
				return "Room not Inserted";
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		System.out.println("Service layer finish");
		return null;
	}

	@Override
	public Room fetchRoomDetails(String roomId) {
		try (Connection connection = getConnection()) {
			return roomDao.selectRoomDetails(connection, roomId);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

}
