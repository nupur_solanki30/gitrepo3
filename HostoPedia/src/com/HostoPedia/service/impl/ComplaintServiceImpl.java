package com.HostoPedia.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import com.HostoPedia.bean.Complaint;
import com.HostoPedia.dao.ComplaintDao;
import com.HostoPedia.dao.FeedbackDao;
import com.HostoPedia.dao.impl.ComplaintDaoImpl;
import com.HostoPedia.dao.impl.FeedbackDaoImpl;
import com.HostoPedia.service.ComplaintService;

public class ComplaintServiceImpl implements ComplaintService{

ComplaintDao complaintDao = new ComplaintDaoImpl();	
	public static Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/hostopedia", "root", "root");

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return connection;
	}
	
	@Override
	public String insertComplaintDetails(Complaint complaint) {
		System.out.println("Complaint Service called");
		try(Connection connection = getConnection())
		{
			int insertRow = complaintDao.saveComplaintDetails(connection, complaint);
			System.out.println("Inserted Row : "+insertRow);
			if(insertRow > 0 )
			{
				
				System.out.println("Complaint Service ended successfully");
				return "insertion successfully";
			}
			else
			{
				System.out.println("complaint Service end");
				return "insertion failed";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Complaint Service end");
		return null;
	}

	@Override
	public List<Complaint> fetchComplaintDetails(int complaintId) {
		// TODO Auto-generated method stub
				try(Connection connection=getConnection())
				{
					return complaintDao.selectComplaintDetails(connection, complaintId);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
	}

	@Override
	public List<Complaint> fetchComplaintDetails() {
		try(Connection connection=getConnection())
		{
			return complaintDao.selectComplaintDetails(connection);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
