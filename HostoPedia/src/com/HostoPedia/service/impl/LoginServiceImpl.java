package com.HostoPedia.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;
import com.HostoPedia.service.LoginService;
import com.HostoPedia.dao.LoginDao;
import com.HostoPedia.dao.impl.LoginDaoImpl;

public class LoginServiceImpl implements LoginService{
	
	LoginDao loginDao=new LoginDaoImpl();
	
	public static Connection getConnection() {
		
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/hostopedia", "root", "root");	
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;		
	}
	
	@Override
	public User validateUser(String email, String pass) {
		System.out.println("Login service called");
		System.out.println("Validate user method called");
		try(Connection connection = getConnection())
		{
			User user = loginDao.getUser(connection, email, pass);
			
			if(user!=null)
			{
				System.out.println(user);
				if(user.getUserRole().equalsIgnoreCase("student"))
				{
					System.out.println("Inside if student!");
					
					Student student = loginDao.getStudent(connection, user.getUserId());
					student.setUserName(user.getUserName());
					student.setUserEmailid(user.getUserEmailid());
					student.setUserContact(user.getUserContact());
					student.setUserAdd(user.getUserAdd());
					student.setUserPass(user.getUserPass());
					student.setUserRole(user.getUserRole());
					student.setUserId(user.getUserId());
					return student;
					
				}
				
				return user;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Validate user method completed");
		System.out.println("Login service complited");
		return null;
	}

	@Override
	public Student validateStudent(int userId) {
		System.out.println("Login service called");
		System.out.println("Validate student method called");

		try(Connection connection = getConnection())
		{
			return loginDao.getStudent(connection, userId);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Validate student method completed");
		System.out.println("Login service complited");
		
		return null;
	}

}
