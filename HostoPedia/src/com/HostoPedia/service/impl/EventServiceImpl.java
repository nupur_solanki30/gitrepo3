package com.HostoPedia.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import com.HostoPedia.bean.Event;
import com.HostoPedia.dao.EventDao;
import com.HostoPedia.dao.impl.EventDaoImpl;
import com.HostoPedia.service.EventService;

public class EventServiceImpl implements EventService{
	EventDao eventDao=new EventDaoImpl();
	
	public static Connection getConnection() {
		
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/hostopedia", "root", "root");	
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return connection;		
	}


	@Override
	public String removeEvent(String eventId) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Service layer called");
		System.out.println("Remove event method called");
		
		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";

		String message = null;

		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername,
				connectionPassword)) {

			int EventId = Integer.parseInt(eventId);
			int deleteRecordId = eventDao.deleteEvent(connection, eventId);
			if (deleteRecordId > 0) {
				message = "Event deleted Successfully";
			} else {
				message = "Event delete failed";
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
		System.out.println("Remove event method completed");
		System.out.println("Service layer completed");
		
		return message;
	}

	@Override
	public ArrayList<Event> fetchEvent() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}

		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";
		
		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername, connectionPassword)) {
			return  eventDao.selectEvent(connection);
		   // return user;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	@Override
	public String modifyEvent(Event event) {
		String message = null;
		try(Connection connection = getConnection()){
			int updatedRecordCount = eventDao.updateEvent(connection,event);
			if(updatedRecordCount>0) {
				message = "Event updated successfully";
			}else {
				message = "Event not updated successfully";
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return message;
	}

	@Override
	public String saveEventDetails(Event event) {
		System.out.println("Service layer called");
		try(Connection connection=getConnection())
		{
			int recordID=eventDao.insertEvent(connection, event);
		
			if(recordID > 0)
				return "Insert Event Successfully";
			else
				return "Event not Inserted";
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		System.out.println("Service layer finish");
		return null;
	}

	@Override
	public Event fetchEventDetails(String eventId) {
	// TODO Auto-generated method stub
		try (Connection connection = getConnection()) {
			return eventDao.selectEventDetails(connection, eventId);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}
}
