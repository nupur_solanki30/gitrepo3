package com.HostoPedia.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import com.HostoPedia.bean.Feedback;
import com.HostoPedia.dao.FeedbackDao;
import com.HostoPedia.dao.impl.FeedbackDaoImpl;
import com.HostoPedia.service.FeedbackService;

public class FeedbackServiceImpl implements FeedbackService {
	FeedbackDao feedbackDao = new FeedbackDaoImpl();
	
	public static Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/hostopedia", "root", "root");

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return connection;
	}
	
	@Override
	public String insertFeedbackDetails(Feedback feedBack) {
		// TODO Auto-generated method stub
		System.out.println("Feedback Service called");
		try(Connection connection = getConnection())
		{
			int insertRow = feedbackDao.saveFeedbackDetails(connection, feedBack);
			System.out.println("Inserted Row : "+insertRow);
			if(insertRow > 0 )
			{
				
				System.out.println("Feedback Service ended successfully");
				return "insertion successfully";
			}
			else
			{
				System.out.println("Feedback Service end");
				return "insertion failed";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Feedback Service end");
		return null;
	}

	@Override
	public List<Feedback> fetchFeedbackDetails(int feedbackId) {
		// TODO Auto-generated method stub
		try(Connection connection=getConnection())
		{
			return feedbackDao.selectFeedbackDetails(connection,feedbackId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Feedback> fetchFeedbackDetails() {
		// TODO Auto-generated method stub
		try(Connection connection=getConnection())
		{
			return feedbackDao.selectFeedbackDetails(connection);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String removeFeedbackDetails(String feedbackId) {
		// TODO Auto-generated method stub
		try(Connection connection=getConnection())
		{
			int deletedRow=feedbackDao.deleteFeedbackDetails(connection,feedbackId);
			
			if(deletedRow>0)
				return "deletion successful!";
			else
				return "deletion failed!";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
