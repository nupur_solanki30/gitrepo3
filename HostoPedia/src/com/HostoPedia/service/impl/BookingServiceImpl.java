package com.HostoPedia.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import com.HostoPedia.bean.Booking;
import com.HostoPedia.dao.BookingDao;
import com.HostoPedia.dao.impl.BookinDaoImpl;
import com.HostoPedia.service.BookingService;

public class BookingServiceImpl implements BookingService {
	BookingDao bookingDao=new BookinDaoImpl();
	public static Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/hostopedia", "root", "root");

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return connection;
	}
	
	@Override
	public String insertBookingDetails(int roomId, int stuId, double price) {
		try(Connection connection=getConnection())
		{
			int insertedRow=bookingDao.saveBookingDetails(connection,roomId,stuId,price);
			if(insertedRow>0)
				return "insertion Successful";
			else
				return "insertion failed";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Booking> fetchBookingDetails() {
		try (Connection connection = getConnection()) {
			return bookingDao.selectBookingDetails(connection);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String updateBookingDetails(Booking booking) {
		System.out.println("Update Booking Details called");
		try(Connection connection=getConnection())
		{
			int updateRow= bookingDao.editBookingDetails(connection,booking);
			if(updateRow>0)
			{
				return "Updation Successfull";
			}
			else
			{
				return "Updation Failed";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String insertBookingDetails(Booking booking) {
		System.out.println("Insert Booking Details called");
		try(Connection connection=getConnection())
		{
			int insertRow=bookingDao.saveBookingDetails(connection,booking);
			if(insertRow >0)
			{
				System.out.println("Insert Booking Details end");
				return "insertion successfull";
			}
			else
			{
				System.out.println("Insert Booking Details end");
				return "insertion fail";
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
