package com.HostoPedia.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import com.HostoPedia.bean.Facility;
import com.HostoPedia.dao.FacilityDao;
import com.HostoPedia.dao.impl.FacilityDaoImpl;
import com.HostoPedia.service.FacilityService;


public class FacilityServiceImpl  implements FacilityService{
	FacilityDao facilityDao=new FacilityDaoImpl();
	
public static Connection getConnection() {
		
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/hostopedia", "root", "root");	
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return connection;		
	}

	@Override
	public String removeFacility(String facilityId) {
		// TODO Auto-generated method stub
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Service layer called");
		System.out.println("Remove facility method called");
		
		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";

		String message = null;

		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername,
				connectionPassword)) {

			int FacilityId = Integer.parseInt(facilityId);
			int deleteRecordId = facilityDao.deleteFacility(connection, facilityId);
			if (deleteRecordId > 0) {
				message = "Facility deleted Successfully";
			} else {
				message = "Facility delete failed";
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
		System.out.println("Remove facility method completed");
		System.out.println("Service layer completed");
		
		return message;
	}

	@Override
	public ArrayList<Facility> fetchFacility() {
		// TODO Auto-generated method stub
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}

		String connectionUrl = "jdbc:mysql://localhost:3306/hostopedia";
		String connectionUsername = "root";
		String connectionPassword = "root";
		
		try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUsername, connectionPassword)) {
			return  facilityDao.selectFacility(connection);
		   // return user;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	@Override
	public String modifyFacility(Facility facility) {
		// TODO Auto-generated method stub
		String message = null;
		try(Connection connection = getConnection()){
			int updatedRecordCount = facilityDao.updateFacility(connection,facility);
			if(updatedRecordCount>0) {
				message = "Facility updated successfully";
			}else {
				message = "Facility not updated successfully";
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return message;
	}

	@Override
	public String saveFacilityDetails(Facility facility) {
		// TODO Auto-generated method stub
		System.out.println("Service layer called");
		try(Connection connection=getConnection())
		{
			int recordID=facilityDao.insertFacility(connection, facility);
		
			if(recordID > 0)
				return "Insert Facility Successfully";
			else
				return "Facility not Inserted";
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		System.out.println("Service layer finish");
		return null;
	}

	@Override
	public Facility fetchFacilityDetails(String facilityId) {
		// TODO Auto-generated method stub
		try (Connection connection = getConnection()) {
			return facilityDao.selectFacilityDetails(connection, facilityId);
	} catch (Exception exception) {
		exception.printStackTrace();
	}
		return null;
	}

}
