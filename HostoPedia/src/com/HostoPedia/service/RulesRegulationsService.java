package com.HostoPedia.service;

import java.util.ArrayList;

import com.HostoPedia.bean.RulesRegulationsBean;


public interface RulesRegulationsService {

	public String removeRules(String rulesId);

	public ArrayList<RulesRegulationsBean> fetchRules();

	public String modifyRules(RulesRegulationsBean rulesRegulationsBean);
	
	public String saveRulesDetails(RulesRegulationsBean rulesRegulationsBean);
	
	public RulesRegulationsBean fetchRulesDetails(String rulesId);
}
