package com.HostoPedia.service;

import java.util.ArrayList;


import com.HostoPedia.bean.StaffDetails;

public interface StaffDetailsService {

	public String removeStaff(String staffId);

	public ArrayList<StaffDetails> fetchStaff();

	public String modifyStaff(StaffDetails staffDetails);
	
	public String saveStaffDetails(StaffDetails staffDetails);
	
	public StaffDetails fetchStaffDetails(String staffId);
}

