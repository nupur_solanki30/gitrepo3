package com.HostoPedia.service;

import java.util.List;

import com.HostoPedia.bean.Booking;

public interface BookingService {
	public String insertBookingDetails(int roomId, int stuId, double price);
	public List<Booking> fetchBookingDetails();
	public String updateBookingDetails(Booking booking);
	public String insertBookingDetails(Booking booking);
}
