package com.HostoPedia.service;

import java.util.List;

import com.HostoPedia.bean.Feedback;

public interface FeedbackService {
	public String insertFeedbackDetails(Feedback feedBack);
	public List<Feedback> fetchFeedbackDetails(int feedbackId);
	public List<Feedback> fetchFeedbackDetails();
	public String removeFeedbackDetails(String feedbackId);
}
