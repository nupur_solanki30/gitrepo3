package com.HostoPedia.service;

import java.util.List;

import com.HostoPedia.bean.Complaint;


public interface ComplaintService {
	public String insertComplaintDetails(Complaint complaint);
	public List<Complaint> fetchComplaintDetails(int complaintId);
	public List<Complaint> fetchComplaintDetails();
}
