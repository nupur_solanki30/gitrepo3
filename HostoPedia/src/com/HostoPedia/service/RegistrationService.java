package com.HostoPedia.service;

import com.HostoPedia.bean.RulesRegulationsBean;
import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;


public interface RegistrationService {
	public String saveUserDetails(User user);
	public String saveStudentDetails(User user, Student student);

}
