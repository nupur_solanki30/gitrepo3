package com.HostoPedia.service;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;

public interface LoginService {
	User validateUser(String email, String pass);
	Student validateStudent(int userId);
}
