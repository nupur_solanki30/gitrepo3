package com.HostoPedia.service;

import java.util.ArrayList;

import com.HostoPedia.bean.Room;

public interface RoomService {
	public String removeRoom(String roomId);

	public ArrayList<Room> fetchRoom();

	public String modifyRoom(Room room);
	
	public String saveRoomDetails(Room room);
	
	public Room fetchRoomDetails(String roomId);
}
