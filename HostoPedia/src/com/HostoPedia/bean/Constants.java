package com.HostoPedia.bean;

public class Constants
{
	public static String delay;
	public static String timetoquery;
	public static String setFrom;
	public static String setPassword;
	public static String emailTO;
	public static String getDelay() {
		return delay;
	}
	public static void setDelay(String delay) {
		Constants.delay = delay;
	}
	public static String getTimetoquery() {
		return timetoquery;
	}
	public static void setTimetoquery(String timetoquery) {
		Constants.timetoquery = timetoquery;
	}
	public static String getSetFrom() {
		return setFrom;
	}
	public static void setSetFrom(String setFrom) {
		Constants.setFrom = setFrom;
	}
	public static String getSetPassword() {
		return setPassword;
	}
	public static void setSetPassword(String setPassword) {
		Constants.setPassword = setPassword;
	}
	public static String getEmailTO() {
		return emailTO;
	}
	public static void setEmailTO(String emailTO) {
		Constants.emailTO = emailTO;
	}
}