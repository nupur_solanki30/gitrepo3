package com.HostoPedia.bean;

public class Food {

	private int FoodId;
	private String FoodName;
	private String FoodType;
	private String Day;
	public int getFoodId() {
		return FoodId;
	}
	public void setFoodId(int foodId) {
		FoodId = foodId;
	}
	public String getFoodName() {
		return FoodName;
	}
	public void setFoodName(String foodName) {
		FoodName = foodName;
	}
	public String getFoodType() {
		return FoodType;
	}
	public void setFoodType(String foodType) {
		FoodType = foodType;
	}
	public String getDay() {
		return Day;
	}
	public void setDay(String day) {
		Day = day;
	}
	
	@Override
	public String toString() {
		return "Food [FoodId=" + FoodId + ", FoodName=" + FoodName + ", FoodType=" + FoodType + ", Day=" + Day + "]";
	}
}
