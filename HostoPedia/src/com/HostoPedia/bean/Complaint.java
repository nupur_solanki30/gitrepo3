package com.HostoPedia.bean;

public class Complaint {
	private int ComptId;
	private String ComptDescription;
	private String RespoDescription;
	private String ComptName;
	private String ComptDate;
	private int UserId;
	private String Status;
	private String ComptSolutionDate;
	private String UserName;
	@Override
	public String toString() {
		return "Complaint [ComptId=" + ComptId + ", ComptDescription=" + ComptDescription + ", RespoDescription="
				+ RespoDescription + ", ComptName=" + ComptName + ", ComptDate=" + ComptDate + ", UserId=" + UserId
				+ ", Status=" + Status + ", ComptSolutionDate=" + ComptSolutionDate + ", UserName=" + UserName + "]";
	}
	public int getComptId() {
		return ComptId;
	}
	public void setComptId(int comptId) {
		ComptId = comptId;
	}
	public String getComptDescription() {
		return ComptDescription;
	}
	public void setComptDescription(String comptDescription) {
		ComptDescription = comptDescription;
	}
	public String getRespoDescription() {
		return RespoDescription;
	}
	public void setRespoDescription(String respoDescription) {
		RespoDescription = respoDescription;
	}
	public String getComptName() {
		return ComptName;
	}
	public void setComptName(String comptName) {
		ComptName = comptName;
	}
	public String getComptDate() {
		return ComptDate;
	}
	public void setComptDate(String comptDate) {
		ComptDate = comptDate;
	}
	public int getUserId() {
		return UserId;
	}
	public void setUserId(int userId) {
		UserId = userId;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getComptSolutionDate() {
		return ComptSolutionDate;
	}
	public void setComptSolutionDate(String comptSolutionDate) {
		ComptSolutionDate = comptSolutionDate;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
}
