package com.HostoPedia.bean;


public class Feedback {
	private int FeedbackId;
	private String FeedbackDescription;
	private String FeedbackRating;
	private String FeedbackDate;
	private int UserId;
	private String UserName;
	
	public int getFeedbackId() {
		return FeedbackId;
	}
	public void setFeedbackId(int feedbackId) {
		FeedbackId = feedbackId;
	}
	public String getFeedbackDescription() {
		return FeedbackDescription;
	}
	public void setFeedbackDescription(String feedbackDescription) {
		FeedbackDescription = feedbackDescription;
	}
	public String getFeedbackRating() {
		return FeedbackRating;
	}
	public void setFeedbackRating(String feedbackRating) {
		FeedbackRating = feedbackRating;
	}
	public String getFeedbackDate() {
		return FeedbackDate;
	}
	public void setFeedbackDate(String feedbackDate) {
		FeedbackDate = feedbackDate;
	}
	public int getUserId() {
		return UserId;
	}
	public void setUserId(int userId) {
		UserId = userId;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	@Override
	public String toString() {
		return "Feedback [FeedbackId=" + FeedbackId + ", FeedbackDescription=" + FeedbackDescription
				+ ", FeedbackRating=" + FeedbackRating + ", FeedbackDate=" + FeedbackDate + ", UserId=" + UserId
				+ ", UserName=" + UserName + "]";
	}
}
