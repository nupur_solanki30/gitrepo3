package com.HostoPedia.bean;

import java.io.InputStream;

public class Room {
	private int RoomId;
	private int StuId;
	private int RoomNo;
	private int FloorNo;
	private InputStream RoomStream;
	private String RoomString;
	private String RoomDesc;
	private int RoomCape;
	private String roomAvailabilityStatus;

	public int getRoomId() {
		return RoomId;
	}
	public void setRoomId(int roomId) {
		RoomId = roomId;
	}
	public int getRoomNo() {
		return RoomNo;
	}
	public void setRoomNo(int roomNo) {
		RoomNo = roomNo;
	}
	public int getFloorNo() {
		return FloorNo;
	}
	public void setFloorNo(int floorNo) {
		FloorNo = floorNo;
	}
	public InputStream getRoomStream() {
		return RoomStream;
	}
	public void setRoomStream(InputStream roomStream) {
		RoomStream = roomStream;
	}
	public String getRoomDesc() {
		return RoomDesc;
	}
	public void setRoomDesc(String roomDesc) {
		RoomDesc = roomDesc;
	}
	public int getRoomCape() {
		return RoomCape;
	}
	public void setRoomCape(int roomCape) {
		RoomCape = roomCape;
	}
	public String getRoomString() {
		return RoomString;
	}
	public void setRoomString(String roomString) {
		RoomString = roomString;
	}
	public int getStuId() {
		return StuId;
	}
	public void setStuId(int stuId) {
		StuId = stuId;
	}
	@Override
	public String toString() {
		return "Room [RoomId=" + RoomId + ", StuId=" + StuId + ", RoomNo=" + RoomNo + ", FloorNo=" + FloorNo
				+ ", RoomStream=" + RoomStream + ", RoomString=" + RoomString + ", RoomDesc=" + RoomDesc + ", RoomCape="
				+ RoomCape + "]";
	}
	public String getRoomAvailabilityStatus() {
		return roomAvailabilityStatus;
	}
	public void setRoomAvailabilityStatus(String roomAvailabilityStatus) {
		this.roomAvailabilityStatus = roomAvailabilityStatus;
	}
}
