package com.HostoPedia.bean;

import java.io.InputStream;

public class Event {
	private int EventId;
	private InputStream EventStream;
	private String EventString;
	private String EventName;
	private String EventDate;
	private String EventDesc;
	public int getEventId() {
		return EventId;
	}
	public void setEventId(int eventId) {
		EventId = eventId;
	}
	public String getEventName() {
		return EventName;
	}
	public void setEventName(String eventName) {
		EventName = eventName;
	}
	public String getEventDate() {
		return EventDate;
	}
	public void setEventDate(String eventDate) {
		EventDate = eventDate;
	}
	public String getEventDesc() {
		return EventDesc;
	}
	public void setEventDesc(String eventDesc) {
		EventDesc = eventDesc;
	}
	public InputStream getEventStream() {
		return EventStream;
	}
	public void setEventStream(InputStream eventStream) {
		EventStream = eventStream;
	}
	public String getEventString() {
		return EventString;
	}
	public void setEventString(String eventString) {
		EventString = eventString;
	}
	@Override
	public String toString() {
		return "Event [EventId=" + EventId + ", EventStream=" + EventStream + ", EventString=" + EventString
				+ ", EventName=" + EventName + ", EventDate=" + EventDate + ", EventDesc=" + EventDesc + "]";
	}
}
