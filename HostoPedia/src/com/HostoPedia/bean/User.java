package com.HostoPedia.bean;

public class User {
	private Integer UserId;
	private String UserName;
	private String UserEmailid;
	private String UserContact;
	private String UserAdd;
	private String UserPass;
	private String UserRole;
	private int Feedback;
	
	public Integer getUserId() {
		return UserId;
	}
	public void setUserId(Integer userId) {
		UserId = userId;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getUserEmailid() {
		return UserEmailid;
	}
	public void setUserEmailid(String userEmailid) {
		UserEmailid = userEmailid;
	}
	public String getUserContact() {
		return UserContact;
	}
	public void setUserContact(String userContact) {
		UserContact = userContact;
	}
	public String getUserAdd() {
		return UserAdd;
	}
	public void setUserAdd(String userAdd) {
		UserAdd = userAdd;
	}
	public String getUserPass() {
		return UserPass;
	}
	public void setUserPass(String userPass) {
		UserPass = userPass;
	}
	public String getUserRole() {
		return UserRole;
	}
	public void setUserRole(String userRole) {
		UserRole = userRole;
	}
	public int getFeedback() {
		return Feedback;
	}
	public void setFeedback(int feedback) {
		Feedback = feedback;
	}
	@Override
	public String toString() {
		return "User [UserId=" + UserId + ", UserName=" + UserName + ", UserEmailid=" + UserEmailid + ", UserContact="
				+ UserContact + ", UserAdd=" + UserAdd + ", UserPass=" + UserPass + ", UserRole=" + UserRole
				+ ", Feedback=" + Feedback + "]";
	}
}


