package com.HostoPedia.bean;

public class StaffDetails {
	
	private int StaffId;
	private String StaffName;
	private String StaffContact;
	private String StaffAdd;
	private String Gender;
	private String Designation;
	public int getStaffId() {
		return StaffId;
	}
	public void setStaffId(int staffId) {
		StaffId = staffId;
	}
	public String getGender() {
		return Gender;
	}
	public void setGender(String gender) {
		Gender = gender;
	}
	public String getDesignation() {
		return Designation;
	}
	public void setDesignation(String designation) {
		Designation = designation;
	}
	public String getStaffName() {
		return StaffName;
	}
	public void setStaffName(String staffName) {
		StaffName = staffName;
	}
	public String getStaffContact() {
		return StaffContact;
	}
	public void setStaffContact(String staffContact) {
		StaffContact = staffContact;
	}
	public String getStaffAdd() {
		return StaffAdd;
	}
	public void setStaffAdd(String staffAdd) {
		StaffAdd = staffAdd;
	}
	@Override
	public String toString() {
		return "StaffDetails [StaffId=" + StaffId + ", StaffName=" + StaffName + ", StaffContact=" + StaffContact
				+ ", StaffAdd=" + StaffAdd + ", Gender=" + Gender + ", Designation=" + Designation + "]";
	}
}
