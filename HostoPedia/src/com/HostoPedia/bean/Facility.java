package com.HostoPedia.bean;

import java.io.InputStream;

public class Facility {

	private int FacilityId;
	private InputStream FacilityStream;
	private String FacilityString;
	private String FacilityName;
	private String Description;
	
	public int getFacilityId() {
		return FacilityId;
	}
	
	public void setFacilityId(int facilityId) {
		FacilityId = facilityId;
	}
	public String getFacilityName() {
		return FacilityName;
	}
	public void setFacilityName(String facilityName) {
		FacilityName = facilityName;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	
	public InputStream getFacilityStream() {
		return FacilityStream;
	}

	public void setFacilityStream(InputStream facilityStream) {
		FacilityStream = facilityStream;
	}

	public String getFacilityString() {
		return FacilityString;
	}

	public void setFacilityString(String facilityString) {
		FacilityString = facilityString;
	}

	@Override
	public String toString() {
		return "Facility [FacilityId=" + FacilityId + ", FacilityStream=" + FacilityStream + ", FacilityString="
				+ FacilityString + ", FacilityName=" + FacilityName + ", Description=" + Description + "]";
	}
}
