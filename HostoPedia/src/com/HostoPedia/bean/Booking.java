package com.HostoPedia.bean;

public class Booking {
	private int bookingId;
	private String paymentId;
	private int stuId;
	private String bookingDate;
	private int bookingStatus;
	private int paymentStatus;
	private int bookingDuration;
	private double ammount;
	private int roomId;
	
	public int getBookingId() {
		return bookingId;
	}
	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	public int getStuId() {
		return stuId;
	}
	public void setStuId(int stuId) {
		this.stuId = stuId;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public int getBookingStatus() {
		return bookingStatus;
	}
	public void setBookingStatus(int bookingStatus) {
		this.bookingStatus = bookingStatus;
	}
	public int getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(int paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public int getBookingDuration() {
		return bookingDuration;
	}
	public void setBookingDuration(int bookingDuration) {
		this.bookingDuration = bookingDuration;
	}
	public double getAmmount() {
		return ammount;
	}
	public void setAmmount(double ammount) {
		this.ammount = ammount;
	}
	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	@Override
	public String toString() {
		return "Booking [bookingId=" + bookingId + ", paymentId=" + paymentId + ", stuId=" + stuId + ", bookingDate="
				+ bookingDate + ", bookingStatus=" + bookingStatus + ", paymentStatus=" + paymentStatus
				+ ", bookingDuration=" + bookingDuration + ", ammount=" + ammount + ", roomId=" + roomId + "]";
	}
	
}
