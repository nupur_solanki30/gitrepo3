package com.HostoPedia.bean;

import java.io.InputStream;

public class Gallery {
	private int GalleryId;
	private InputStream GalleryStream;
	private String GalleryString;
	
	public int getGalleryId() {
		return GalleryId;
	}
	public void setGalleryId(int galleryId) {
		GalleryId = galleryId;
	}
	public InputStream getGalleryStream() {
		return GalleryStream;
	}
	public void setGalleryStream(InputStream galleryStream) {
		GalleryStream = galleryStream;
	}
	public String getGalleryString() {
		return GalleryString;
	}
	public void setGalleryString(String galleryString) {
		GalleryString = galleryString;
	}
	@Override
	public String toString() {
		return "Gallery [GalleryId=" + GalleryId + ", GalleryStream=" + GalleryStream + ", GalleryString="
				+ GalleryString + "]";
	}
}
