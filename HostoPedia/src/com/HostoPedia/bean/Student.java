package com.HostoPedia.bean;

import java.io.InputStream;
//import java.io.InputStream;

//import com.hostelManagementSystem.bean.User;

public class Student extends User{
	private Integer UserId;
	private Integer StuId;
	private String  StuClgName;
	private InputStream StuDocStream;
	private String StuDocString;
	private String StuDob;
	private String StuFatherName;
	private String StuMotherName;
	private Integer RoomId;
	
	public Integer getUserId() {
		return UserId;
	}
	public void setUserId(Integer userId) {
		UserId = userId;
	}
	public String getStuClgName() {
		return StuClgName;
	}
	public void setStuClgName(String stuClgName) {
		StuClgName = stuClgName;
	}
	public String getStuFatherName() {
		return StuFatherName;
	}
	public void setStuFatherName(String stuFatherName) {
		StuFatherName = stuFatherName;
	}
	public String getStuMotherName() {
		return StuMotherName;
	}
	public void setStuMotherName(String stuMotherName) {
		StuMotherName = stuMotherName;
	}
	public Integer getStuId() {
		return StuId;
	}
	public void setStuId(Integer stuId) {
		StuId = stuId;
	}
	public String getStuDob() {
		return StuDob;
	}
	public void setStuDob(String stuDob) {
		StuDob = stuDob;
	}
	public InputStream getStuDocStream() {
		return StuDocStream;
	}
	public void setStuDocStream(InputStream stuDocStream) {
		StuDocStream = stuDocStream;
	}
	public String getStuDocString() {
		return StuDocString;
	}
	public void setStuDocString(String stuDocString) {
		StuDocString = stuDocString;
	}
	public Integer getRoomId() {
		return RoomId;
	}
	public void setRoomId(Integer roomId) {
		RoomId = roomId;
	}
	
	
	@Override
	public String toString() {
		return "Student [UserId=" + UserId + ", StuId=" + StuId + ", StuClgName=" + StuClgName + ", StuDocStream="
				+ StuDocStream + ", StuDocString=" + StuDocString + ", StuDob=" + StuDob + ", StuFatherName="
				+ StuFatherName + ", StuMotherName=" + StuMotherName + ", RoomId=" + RoomId + "]";
	}
}


