package com.HostoPedia.bean;

public class RulesRegulationsBean {

	private int RulesId;
	private String Description;
	private String ApplyDate;
	public int getRulesId() {
		return RulesId;
	}
	public void setRulesId(int rulesId) {
		RulesId = rulesId;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getApplyDate() {
		return ApplyDate;
	}
	public void setApplyDate(String applyDate) {
		ApplyDate = applyDate;
	}
	
}
