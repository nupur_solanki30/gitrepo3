package com.HostoPedia.bean;

public class Register {
	private Integer UserId;
	private String UserName;
	private String UserEmailid;
	private String UserContact;
	private String UserAdd;
	private String UserPass;
	private String UserRole;
	public Integer getUserId() {
		return UserId;
	}
	public void setUserId(Integer userId) {
		UserId = userId;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getUserEmailid() {
		return UserEmailid;
	}
	public void setUserEmailid(String userEmailid) {
		UserEmailid = userEmailid;
	}
	public String getUserContact() {
		return UserContact;
	}
	public void setUserContact(String userContact) {
		UserContact = userContact;
	}
	public String getUserAdd() {
		return UserAdd;
	}
	public void setUserAdd(String userAdd) {
		UserAdd = userAdd;
	}
	public String getUserPass() {
		return UserPass;
	}
	public void setUserPass(String userPass) {
		UserPass = userPass;
	}
	public String getUserRole() {
		return UserRole;
	}
	public void setUserRole(String userRole) {
		UserRole = userRole;
	}
	
	@Override
	public String toString() {
		return "Register [UserId=" + UserId + ", UserName=" + UserName + ", UserEmailid=" + UserEmailid
				+ ", UserContact=" + UserContact + ", UserAdd=" + UserAdd + ", UserPass=" + UserPass + ", UserRole="
				+ UserRole + "]";
	}
}



