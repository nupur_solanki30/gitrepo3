package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import com.HostoPedia.bean.Room;
import com.HostoPedia.service.RoomService;
import com.HostoPedia.service.impl.RoomServiceImpl;

/**
 * Servlet implementation class RoomServlet
 */
public class RoomServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	RoomService roomService=new RoomServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RoomServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Room Servlet called");
		
		Part part = request.getPart("roomImg");
		
		Room room = new Room();
		String roomNo = request.getParameter("roomNo");
		String floorNo = request.getParameter("floorNo");
		if(null!=part) {
			System.out.println("Image Name : " + part.getName());
			System.out.println("Image Name 2 : " + part.getSubmittedFileName());
			System.out.println("Image Size :: " + part.getSize());
			room.setRoomStream(part.getInputStream());
		}
		String description = request.getParameter("description");
		String roomCape = request.getParameter("capacity");
		
		System.out.println(roomNo);
		System.out.println(description);
		System.out.println(floorNo);
		System.out.println(roomCape);
		room.setRoomNo(Integer.parseInt(roomNo));
		room.setFloorNo(Integer.parseInt(floorNo));
		room.setRoomDesc(description);
		room.setRoomCape(Integer.parseInt(roomCape));
		
		String RoomMessage = roomService.saveRoomDetails(room);
		System.out.println("Servlet execution complete");
		response.getWriter().append(RoomMessage);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayRoomServlet");	
		requestDispatcher.forward(request, response);
	}

}
