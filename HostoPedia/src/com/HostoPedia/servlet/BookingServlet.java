package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;
import com.HostoPedia.service.BookingService;
import com.HostoPedia.service.impl.BookingServiceImpl;

/**
 * Servlet implementation class BookingServlet
 */
public class BookingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	BookingService bookingService=new BookingServiceImpl();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookingServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String duration=request.getParameter("duration");
		String amount=request.getParameter("amount");
		
		System.out.println("duration"+ duration);
        System.out.println("amount"+amount);
        
        HttpSession httpSession=request.getSession(false);
		//Student student =new Student();
			if(null!=httpSession)
			{
		 	User user=(User) httpSession.getAttribute("loginBean");
		 	String msg=bookingService.insertBookingDetails( 1,user.getUserId(),new Double(amount));

		 	System.out.println(user.getUserId());
			
			}
			
        
        RequestDispatcher requestDispatcher=request.getRequestDispatcher("TxnTest.jsp");
     requestDispatcher.forward(request, response);
	}
}
	// TODO Auto-generated method stub
		//doGet(request, response);
//		HttpSession httpSession=request.getSession(false);
//		Student student =new Student();
//			if(null!=httpSession)
//			{
//		 	User user=(User) httpSession.getAttribute("loginBean");
//			}
//			
//			
//			String floorNo=request.getParameter("floorNo");
//			String capacity=request.getParameter("capacity");
//		int roomId = student.getRoomId();
//		System.out.println("RoomId "+roomId);
//		int stuId=student.getStuId();
//		String memberName=student.);
//		System.out.println("StuId "+stuId);
//		System.out.println("floorno"+floorNo );
//		System.out.println("capacity"+capacity);
//		
//		double price = (Double) null;
//		String msg=bookingService.insertBookingDetails(roomId,stuId,price);
//		request.setAttribute("stuId", stuId);
//		request.setAttribute("roomId", roomId);
//		request.setAttribute("price", price);
//		
//		RequestDispatcher requestDispatcher=request.getRequestDispatcher("TxnTest.jsp");
//		//requestDispatcher.forward(request, response);
	//}
//}
