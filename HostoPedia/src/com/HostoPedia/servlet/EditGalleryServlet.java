package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.Gallery;
import com.HostoPedia.service.GalleryService;
import com.HostoPedia.service.impl.GalleryServiceImpl;

/**
 * Servlet implementation class EditGalleryServlet
 */
public class EditGalleryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    GalleryService galleryService = new GalleryServiceImpl();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditGalleryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String galleryId = request.getParameter("galleryId");
		System.out.println(galleryId);
		Gallery gallery = null; 
				
		gallery = galleryService.fetchGalleryDetails(galleryId);
		
		gallery.setGalleryId(Integer.parseInt(galleryId));
		
		request.setAttribute("gallery",gallery);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("EditGallery.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
