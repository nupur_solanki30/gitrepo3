package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.RulesRegulationsBean;
import com.HostoPedia.service.RulesRegulationsService;
import com.HostoPedia.service.impl.RulesRegulationsServiceImpl;

/**
 * Servlet implementation class UpdateRulesServlet
 */
public class UpdateRulesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      RulesRegulationsService rulesRegulationsService = new RulesRegulationsServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateRulesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		int rulesId = Integer.parseInt(request.getParameter("rulesId"));
		System.out.println("Update rules servlet called"+rulesId);
		String description = request.getParameter("desc");
		String applyDate = request.getParameter("date");
		
		RulesRegulationsBean rulesRegulationsBean = new RulesRegulationsBean();
		rulesRegulationsBean.setRulesId(rulesId);
		rulesRegulationsBean.setDescription(description);
		rulesRegulationsBean.setApplyDate(applyDate);
		
		String rulesModifyMessage = rulesRegulationsService.modifyRules(rulesRegulationsBean);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayRulesServlet");
		requestDispatcher.forward(request, response);
		//doGet(request, response);
	}

}
