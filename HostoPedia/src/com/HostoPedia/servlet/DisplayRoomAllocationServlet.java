package com.HostoPedia.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.RoomAllocationService;
import com.HostoPedia.service.impl.RoomAllocationServiceImpl;

/**
 * Servlet implementation class DisplayRoomAllocationServlet
 */
public class DisplayRoomAllocationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	RoomAllocationService roomAllocationService = new RoomAllocationServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayRoomAllocationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ArrayList<com.HostoPedia.bean.Room> roomList = roomAllocationService.fetchRoom();
		   System.out.println(roomList);
	      
		   if(roomList!=null)
		     {
	        	 RequestDispatcher requestdispatcher=request.getRequestDispatcher("RoomAllocation.jsp");
	        	 request.setAttribute("roomList", roomList);
	        
	        	 requestdispatcher.forward(request, response);
		     }
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
