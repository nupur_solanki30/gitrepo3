package com.HostoPedia.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.HostoPedia.bean.Complaint;
import com.HostoPedia.bean.Feedback;
import com.HostoPedia.bean.User;
import com.HostoPedia.service.ComplaintService;
import com.HostoPedia.service.FeedbackService;
import com.HostoPedia.service.impl.ComplaintServiceImpl;
import com.HostoPedia.service.impl.FeedbackServiceImpl;

/**
 * Servlet implementation class ComplaintServlet
 */
public class ComplaintServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	ComplaintService complaintService = new ComplaintServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ComplaintServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Complaint servlet called!");
		HttpSession httpSession=request.getSession(false);
		User user = null;
			if(null!=httpSession)
			{
			 	user=(User) httpSession.getAttribute("loginBean");
			}
			
//		int branchId=user.getBranch();
//		System.out.println("branchId "+branchId);
			
		int userId = user.getUserId();
		System.out.println("userId "+userId);
		
		String comptName = request.getParameter("complaintname");
		System.out.println(comptName);
		String ComptDesc = request.getParameter("message");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String dateTime = formatter.format(date);
		System.out.println(dateTime);
		
		Complaint complaint = new Complaint();
		complaint.setComptName(comptName);
	    complaint.setComptDescription(ComptDesc);
		
		complaint.setComptDate(dateTime);
		complaint.setUserId(userId);
		
		String msg = complaintService.insertComplaintDetails(complaint);
		System.out.println(msg);
		response.sendRedirect("Student.jsp");
	}

}
