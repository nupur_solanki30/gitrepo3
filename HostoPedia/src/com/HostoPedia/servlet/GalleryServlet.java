package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.HostoPedia.bean.Gallery;
import com.HostoPedia.bean.Student;
import com.HostoPedia.service.GalleryService;
import com.HostoPedia.service.impl.GalleryServiceImpl;

/**
 * Servlet implementation class GalleryServlet
 */
public class GalleryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    GalleryService galleryService = new GalleryServiceImpl();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GalleryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Gallery Servlet called");
		
		Part part = request.getPart("galleryImg");
		
		Gallery gallery = new Gallery();
		if(null!=part) {
			System.out.println("Image Name" + part.getName());
			System.out.println("Image Name 2" + part.getSubmittedFileName());
			System.out.println("Image Size :: " + part.getSize());
			gallery.setGalleryStream(part.getInputStream());
		}
		
		String eventMessage = galleryService.saveGalleryDetails(gallery);
		System.out.println("Servlet execution complete");
		response.getWriter().append(eventMessage);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayGalleryServlet");	
		requestDispatcher.forward(request, response);
		doGet(request, response);
	}

}
