package com.HostoPedia.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.HostoPedia.bean.Room;
import com.HostoPedia.service.RoomService;
import com.HostoPedia.service.impl.RoomServiceImpl;

/**
 * Servlet implementation class RoomAllocationServlet
 */
public class RoomAllocationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    RoomService roomService = new RoomServiceImpl();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RoomAllocationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		System.out.println("Room  allocation Servlet called");
		
		Room room = new Room();
		String stuId = request.getParameter("stuId");
		String roomNo = request.getParameter("roomNo");
		String floorNo = request.getParameter("floorNo");
		String roomCape = request.getParameter("capacity");
		
		System.out.println(stuId);
		System.out.println(roomNo);
		System.out.println(floorNo);
		System.out.println(roomCape);
		
		room.setStuId(Integer.parseInt(stuId));
		room.setRoomNo(Integer.parseInt(roomNo));
		room.setFloorNo(Integer.parseInt(floorNo));
		room.setRoomCape(Integer.parseInt(roomCape));
		
		ArrayList <Room> RoomAllocationMessage = roomService.fetchRoom();
		System.out.println("Servlet execution complete");
		//response.getWriter().append(RoomAllocationMessage);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayRoomServlet");	
		requestDispatcher.forward(request, response);
		
		//doGet(request, response);
	}
}
