package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;
import com.HostoPedia.service.ProfileService;
import com.HostoPedia.service.impl.ProfileServiceImpl;

/**
 * Servlet implementation class EditStudentProfile
 */
public class EditStudentProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ProfileService profileService = new ProfileServiceImpl();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditStudentProfile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		System.out.println("Update profile Servlet called");
		String UserName=request.getParameter("fullname");
		String UserEmailid=request.getParameter("emailid");
		String UserContact=request.getParameter("contactno");
		String UserAdd=request.getParameter("address");
		String UserRole=request.getParameter("role");
		int userId = Integer.parseInt(request.getParameter("userid"));
		String collegeName = request.getParameter("collegename");
		String fatherName = request.getParameter("fathername");
		String motherName = request.getParameter("mothername");
		String dob = request.getParameter("dob");
		
		System.out.println("User id ========= "+userId);
	//	int stuId = Integer.parseInt(request.getParameter("stuid"));
		
		Student student = new Student();
		Part part = request.getPart("doc");
		if(part!=null && part.getSize()>0) {
			student.setStuDocStream(part.getInputStream());
		}
		
		User user = new User();
		student.setUserName(UserName);
		student.setUserEmailid(UserEmailid);
		student.setUserContact(UserContact);
		student.setUserAdd(UserAdd);
		student.setUserRole(UserRole);
		student.setUserId(userId);
//		
		//student.setStuId(stuId);
		student.setStuClgName(collegeName);
		student.setStuDob(dob);
		student.setStuFatherName(fatherName);
		student.setStuMotherName(motherName);
		
		String updateMessage = profileService.modifyUserDetails(user, student);
		
		HttpSession httpSession=request.getSession();
		User user1 = (User) httpSession.getAttribute("loginBean");
		Student student1 = (Student) httpSession.getAttribute("loginBean");
		
		user1.setUserName(UserName);
		user1.setUserEmailid(UserEmailid);
		user1.setUserContact(UserContact);
		user1.setUserAdd(UserAdd);
		user1.setUserRole(UserRole);
		user1.setUserId(userId);
		
		student1.setStuClgName(collegeName);
		student1.setStuDob(dob);
		student1.setStuFatherName(fatherName);
		student1.setStuMotherName(motherName);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("EditStudentProfile.jsp");
		requestDispatcher.forward(request, response);
		
		System.out.println("Profile servlet completed");
		
		//doGet(request, response);
	}

}
