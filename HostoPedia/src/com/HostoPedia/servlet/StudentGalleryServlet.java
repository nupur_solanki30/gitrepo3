package com.HostoPedia.servlet;

import java.util.List;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.Gallery;
import com.HostoPedia.service.GalleryService;
import com.HostoPedia.service.impl.GalleryServiceImpl;

/**
 * Servlet implementation class StudentGalleryServlet
 */
public class StudentGalleryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	GalleryService galleryService = new GalleryServiceImpl();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentGalleryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<Gallery> galleryList=galleryService.fetchGallery();
		request.setAttribute("galleryList", galleryList);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("gallery_1.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
