package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.StaffDetails;
import com.HostoPedia.service.StaffDetailsService;
import com.HostoPedia.service.impl.StaffDetailsServiceImpl;

/**
 * Servlet implementation class UpdateStaffServlet
 */
public class UpdateStaffServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	StaffDetailsService staffDetailsService=new StaffDetailsServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateStaffServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int staffId = Integer.parseInt(request.getParameter("staffId"));
		System.out.println("Update staff servlet called "+staffId);
		
		String Name = request.getParameter("Name");
		String Contact = request.getParameter("Contact");
		String Address = request.getParameter("Address");
		String Gender = request.getParameter("Gender");
		String Designation = request.getParameter("Designation");
		
		System.out.println("Gender = "+Gender);
		StaffDetails staffDetails = new StaffDetails();
		staffDetails.setStaffId(staffId);
		staffDetails.setStaffName(Name);
		staffDetails.setStaffContact(Contact);
		staffDetails.setStaffAdd(Address);
		staffDetails.setGender(Gender);
		staffDetails.setDesignation(Designation);
		
		String staffModifyMessage = staffDetailsService.modifyStaff(staffDetails);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayStaffServlet");
		requestDispatcher.forward(request, response);
		//doGet(request, response);
	}
}
