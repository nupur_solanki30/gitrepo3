package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.HostoPedia.service.LoginService;
import com.HostoPedia.service.impl.LoginServiceImpl;
import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	LoginService loginService = new LoginServiceImpl();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		System.out.println("Login servlet called");
		String email = request.getParameter("email");
	    String pass = request.getParameter("pass");
	    System.out.println("Email : "+email);
	    System.out.println("Password : "+pass);
	    
		User user = loginService.validateUser(email,pass);
		
		
		if(user==null)
		{
			String msg="Please enter valid username and password!";
			System.out.println("Inside login!");
			request.setAttribute("msg", msg);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("Login.jsp");
			requestDispatcher.forward(request, response);
		}
		else
		{
			HttpSession httpSession = request.getSession();
			if("student".equalsIgnoreCase(user.getUserRole())){
				Student student = (Student) user;
			//	for(Student s: student) {
				//System.out.println("UserId");
				//}
				httpSession.setAttribute("loginBean", user);
				response.sendRedirect("Student.jsp");
			}
			else if("warden".equalsIgnoreCase(user.getUserRole())){
				httpSession.setAttribute("loginBean", user);
				response.sendRedirect("Warden.jsp");
			}
			else {
				httpSession.setAttribute("loginBean", user);
				response.sendRedirect("Admin.jsp");
			}
		}
		System.out.println("login servlet completed");
		
		//doGet(request, response);
	}

}
