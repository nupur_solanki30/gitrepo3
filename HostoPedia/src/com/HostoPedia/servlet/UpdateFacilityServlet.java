package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.HostoPedia.bean.Facility;
import com.HostoPedia.bean.RulesRegulationsBean;
import com.HostoPedia.service.FacilityService;
import com.HostoPedia.service.impl.FacilityServiceImpl;

/**
 * Servlet implementation class UpdateFacilityServlet
 */
public class UpdateFacilityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	FacilityService facilityService=new FacilityServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateFacilityServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int facilityId = Integer.parseInt(request.getParameter("facilityId"));
		System.out.println("Update facility servlet called"+facilityId);
		Facility facility = new Facility();
		
		String facilityName = request.getParameter("facilityName");
		String Description = request.getParameter("description");
		Part part = request.getPart("facilityImage");
		if(null!=part) {
			System.out.println("Image Name : " + part.getName());
			System.out.println("Image Name 2 : " + part.getSubmittedFileName());
			System.out.println("Image Size :: " + part.getSize());
			facility.setFacilityStream(part.getInputStream());
		}
		
		facility.setFacilityId(facilityId);
		facility.setFacilityName(facilityName);
		facility.setDescription(Description);
		
		String facilityModifyMessage = facilityService.modifyFacility(facility);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayFacilityServlet");
		requestDispatcher.forward(request, response);
		//doGet(request, response);
	}
}

