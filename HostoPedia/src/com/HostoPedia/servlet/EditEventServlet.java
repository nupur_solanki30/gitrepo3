package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.Event;
import com.HostoPedia.service.EventService;
import com.HostoPedia.service.impl.EventServiceImpl;

/**
 * Servlet implementation class EditEventServlet
 */
public class EditEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	EventService eventService = new EventServiceImpl();    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditEventServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String eventId = request.getParameter("eventId");
		System.out.println(eventId);
		Event event = null; 
				
		event = eventService.fetchEventDetails(eventId);
		
		event.setEventId(Integer.parseInt(eventId));
		
		request.setAttribute("event",event);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("EditEvent.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
