package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.Food;
import com.HostoPedia.bean.RulesRegulationsBean;
import com.HostoPedia.service.FoodService;
import com.HostoPedia.service.impl.FoodServiceImpl;

/**
 * Servlet implementation class UpdateFoodServlet
 */
public class UpdateFoodServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	FoodService foodService = new FoodServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateFoodServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int foodId = Integer.parseInt(request.getParameter("foodId"));
		System.out.println("Update food servlet called "+foodId);
		String foodName = request.getParameter("foodName");
		String foodType = request.getParameter("foodType");
		String day = request.getParameter("foodDay");
		
		Food food = new Food();
		
		food.setFoodName(foodName);
		food.setFoodType(foodType);
		food.setDay(day);
		
		String foodModifyMessage = foodService.modifyFood(food);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayFoodServlet");
		requestDispatcher.forward(request, response);
	}

}
