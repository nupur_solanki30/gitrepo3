  package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.UserService;
import com.HostoPedia.service.impl.UserServiceImpl;
import com.email.durgesh.Email;


/**
 * Servlet implementation class ForgetpassServlet
 */
public class ForgetpassServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	UserService userService = new UserServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ForgetpassServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Forget pass servlet called");
		String email = request.getParameter("email");
		System.out.println(email);
		
  	    char[] msg=userService.ValidateUser(email);
		String str = String.valueOf(msg);
		System.out.println(str);	
	
		
		if(str.equalsIgnoreCase("Invalid"))
		{
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("Forgetpassword.jsp");
			requestDispatcher.forward(request, response);
		}
		else
		{
			System.out.println("Forget pass servlet inside else");
			try
			{
				System.out.println("Forget pass servlet inside try");
				Email emailHost =new Email("Hostopedia18@gmail.com", "18hostopedia");
				emailHost.setFrom("Hostopedia18@gmail.com", "Hostopedia");
				emailHost.setSubject("Password Recovery at Hostopedia");
				emailHost.setContent("<p>Your OTP is "+str+"!</p>","text/html");
				emailHost.addRecipient(email);
				emailHost.send();
				request.setAttribute("email",email );
				request.setAttribute("str", str);
				
				RequestDispatcher requestDispatcher=request.getRequestDispatcher("Otp.jsp");
				requestDispatcher.forward(request, response);
				
			}
			
			catch (Exception e) {
				// TODO: handle exception
				System.out.println("mail sending failed");
			}
			
		}
		System.out.println("Forget pass servlet end");
	}

}
