package com.HostoPedia.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.HostoPedia.bean.Complaint;
import com.HostoPedia.bean.Feedback;
import com.HostoPedia.bean.User;
import com.HostoPedia.service.ComplaintService;
import com.HostoPedia.service.FeedbackService;
import com.HostoPedia.service.impl.ComplaintServiceImpl;
import com.HostoPedia.service.impl.FeedbackServiceImpl;

/**
 * Servlet implementation class DisplayComplaintToAdmin
 */
public class DisplayComplaintToAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	FeedbackService feedbackService = new FeedbackServiceImpl(); 
	ComplaintService complaintService = new ComplaintServiceImpl();
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayComplaintToAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession httpSession=request.getSession(false);
		User user =null;
			if(null!=httpSession)
			{
			 	user=(User) httpSession.getAttribute("loginBean");
			}
			
//		int feedbackId=user.getFeedback();
//		System.out.println("feedbackId "+feedbackId);
		
		List<Complaint> complaintList=complaintService.fetchComplaintDetails();
		request.setAttribute("complaintList", complaintList);
		RequestDispatcher requestDispatcher=request.getRequestDispatcher("DisplayComplaintToAW.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
