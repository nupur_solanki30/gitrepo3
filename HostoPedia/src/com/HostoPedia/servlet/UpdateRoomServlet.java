package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.HostoPedia.bean.Room;
import com.HostoPedia.service.RoomService;
import com.HostoPedia.service.impl.RoomServiceImpl;

/**
 * Servlet implementation class UpdateRoomServlet
 */
public class UpdateRoomServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    RoomService roomService = new RoomServiceImpl();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateRoomServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int roomId = Integer.parseInt(request.getParameter("roomId"));
		System.out.println("Update Room servlet called "+roomId);
		
		Room room = new Room();
		
		Part part = request.getPart("roomImage");
		if(null!=part) {
			System.out.println("Image Name : " + part.getName());
			System.out.println("Image Name 2 : " + part.getSubmittedFileName());
			System.out.println("Image Size :: " + part.getSize());
			room.setRoomStream(part.getInputStream());
		}
		String roomNo = request.getParameter("roomNo");
		String floorNo = request.getParameter("floorNo");
		String Description = request.getParameter("desc");
		String Capacity = request.getParameter("cape");
		
		room.setRoomId(roomId);
		room.setRoomNo(Integer.parseInt(roomNo));
		room.setFloorNo(Integer.parseInt(floorNo));
		room.setRoomDesc(Description);
		room.setRoomCape(Integer.parseInt(Capacity));
		
		String roomModifyMessage = roomService.modifyRoom(room);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayRoomServlet");
		requestDispatcher.forward(request, response);
		//doGet(request, response);
	}

}
