package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.HostoPedia.bean.Facility;
import com.HostoPedia.service.FacilityService;
import com.HostoPedia.service.impl.FacilityServiceImpl;

/**
 * Servlet implementation class FacilityServlet
 */
public class FacilityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	FacilityService facilityService=new FacilityServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FacilityServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Facility Servlet called");
		
		String facilityName = request.getParameter("facilityName");
		String description = request.getParameter("description");
		Part part = request.getPart("facilityImg");
		
		Facility facility = new Facility();
		if(null!=part) {
			System.out.println("Image Name : " + part.getName());
			System.out.println("Image Name 2 : " + part.getSubmittedFileName());
			System.out.println("Image Size :: " + part.getSize());
			facility.setFacilityStream(part.getInputStream());
		}
		
		System.out.println(description);
		System.out.println(facilityName);
		
		facility.setFacilityName(facilityName);
		facility.setDescription(description);

		String facilityMessage = facilityService.saveFacilityDetails(facility);
		System.out.println("Servlet execution complete");
		response.getWriter().append(facilityMessage);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayFacilityServlet");	
		requestDispatcher.forward(request, response);

	}
}
