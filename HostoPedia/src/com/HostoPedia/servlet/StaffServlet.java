package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.HostoPedia.bean.StaffDetails;
import com.HostoPedia.service.StaffDetailsService;
import com.HostoPedia.service.impl.StaffDetailsServiceImpl;

/**
 * Servlet implementation class StaffServlet
 */
public class StaffServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	StaffDetailsService staffDetailsService=new StaffDetailsServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StaffServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Staff Servlet called");
		
		String Name = request.getParameter("Name");
		String Contact = request.getParameter("Contact");
		String Address = request.getParameter("Address");
		String Gender = request.getParameter("Gender");
		String Designation = request.getParameter("Designation");
		
		System.out.println(Name);
		System.out.println(Contact);
		System.out.println(Address);
		System.out.println(Gender);
		System.out.println(Designation);
		
		StaffDetails staffDetails = new StaffDetails();
		
		staffDetails.setStaffName(Name);
		staffDetails.setStaffContact(Contact);
		staffDetails.setStaffAdd(Address);
		staffDetails.setGender(Gender);
		staffDetails.setDesignation(Designation);
		
		String staffMessage = staffDetailsService.saveStaffDetails(staffDetails);
		System.out.println("Servlet execution complete");
		response.getWriter().append(staffMessage);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayStaffServlet");	
		requestDispatcher.forward(request, response);

	}
}
