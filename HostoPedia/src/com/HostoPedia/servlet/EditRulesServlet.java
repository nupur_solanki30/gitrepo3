package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.RulesRegulationsBean;
import com.HostoPedia.service.RulesRegulationsService;
import com.HostoPedia.service.impl.RulesRegulationsServiceImpl;

/**
 * Servlet implementation class EditRulesServlet
 */
public class EditRulesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    RulesRegulationsService rulesRegulationsService=new RulesRegulationsServiceImpl();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditRulesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String rulesId = request.getParameter("rulesId");
		System.out.println(rulesId);
		RulesRegulationsBean rulesRegulationsBean = null; 
				
		rulesRegulationsBean = rulesRegulationsService.fetchRulesDetails(rulesId);
		
		rulesRegulationsBean.setRulesId(Integer.parseInt(rulesId));
		
		request.setAttribute("rulesRegulations", rulesRegulationsBean);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("EditRules.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
