package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.User;
import com.HostoPedia.service.UserService;
import com.HostoPedia.service.impl.UserServiceImpl;

/**
 * Servlet implementation class EditStudentDetails
 */
public class EditStudentDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
	UserService userService = new UserServiceImpl();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditStudentDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		System.out.println("servlet called");
		String userId = request.getParameter("userId");
		System.out.println(userId);
		//  User user=(User) UserService.fetchUserDetails(userId);
		User user = null;
		user=userService.fetchStudentDetails(userId);
		user.setUserId(Integer.parseInt(userId));
		request.setAttribute("user", user);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("EditStudent.jsp");
		requestDispatcher.forward(request, response);
		
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
