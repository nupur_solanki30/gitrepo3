package com.HostoPedia.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.StaffDetailsService;
import com.HostoPedia.service.impl.StaffDetailsServiceImpl;

/**
 * Servlet implementation class DisplayStaffServlet
 */
public class DisplayStaffServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	StaffDetailsService staffDetailsService=new StaffDetailsServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayStaffServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ArrayList<com.HostoPedia.bean.StaffDetails> staffDetailsList = staffDetailsService.fetchStaff();
		
		   System.out.println(staffDetailsList);
	      
		   if(staffDetailsList!=null)
		     {
	        	 RequestDispatcher requestdispatcher=request.getRequestDispatcher("StaffDetails.jsp");
	        	 request.setAttribute("staffDetailsList", staffDetailsList);
	        
	        	 requestdispatcher.forward(request, response);
		     }
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
