package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.FoodService;
import com.HostoPedia.service.impl.FoodServiceImpl;


/**
 * Servlet implementation class RemoveFoodServlet
 */
public class RemoveFoodServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	FoodService foodService = new FoodServiceImpl();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveFoodServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Remove food sevlet called");
		String foodId = request.getParameter("foodId");
		
		String removeFoodMessage = foodService.removeFood(foodId);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayFoodServlet");
		requestDispatcher.forward(request, response);
		
		System.out.println("Remove food sevlet completed");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
