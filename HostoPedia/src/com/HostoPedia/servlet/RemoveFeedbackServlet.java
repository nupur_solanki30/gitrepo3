package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.FeedbackService;
import com.HostoPedia.service.impl.FeedbackServiceImpl;

/**
 * Servlet implementation class RemoveFeedbackServlet
 */
public class RemoveFeedbackServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    FeedbackService feedbackService = new FeedbackServiceImpl();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveFeedbackServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Remove feedback sevlet called");
		String feedbackId = request.getParameter("feedbackId");
		
		String removeFoodMessage = feedbackService.removeFeedbackDetails(feedbackId);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayFeedbackToAdmin");
		requestDispatcher.forward(request, response);
		
		System.out.println("Remove feedback sevlet completed");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
