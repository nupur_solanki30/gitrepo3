package com.HostoPedia.servlet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.User;
import com.HostoPedia.service.UserService;
import com.HostoPedia.service.impl.UserServiceImpl;
//import com.hostelManagementSystem.bean.Student;
//import com.hostelManagementSystem.bean.Student;



/**
 * Servlet implementation class DisplayAlUserDetails
 */
public class DisplayAlUserDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
	UserService userservice=new UserServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayAlUserDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 ArrayList<User> userList=userservice.fetchAllUserDetails();
		   System.out.println(userList);
	         if(userList!=null)
		     {
	        	 RequestDispatcher requestdispatcher=request.getRequestDispatcher("User.jsp");
	        	 request.setAttribute("userList",userList );
	        
	        	 requestdispatcher.forward(request, response);
		     }
	         List<com.HostoPedia.bean.Student>studentList=userservice.fetchStudentDetails();
	  	   System.out.println(studentList);
	           if(studentList!=null)
	  	     {
	          	 RequestDispatcher requestdispatcher=request.getRequestDispatcher("User.jsp");
	          	 request.setAttribute("studentList",studentList );
	          	// request.setAttribute("StudentList", studentList);
	          	 requestdispatcher.forward(request, response);
	  	     }
		}
		

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
