package com.HostoPedia.servlet;

import java.awt.Event;
import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.Facility;
import com.HostoPedia.service.EventService;
import com.HostoPedia.service.impl.EventServiceImpl;

/**
 * Servlet implementation class StudentEventServlet
 */
public class StudentEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	EventService eventService=new EventServiceImpl();
			
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentEventServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//List<Event> eventList=eventService.fetchEvent();
		List<com.HostoPedia.bean.Event> eventList=eventService.fetchEvent();
		request.setAttribute("eventList", eventList);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("event_1.jsp");
		requestDispatcher.forward(request, response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
