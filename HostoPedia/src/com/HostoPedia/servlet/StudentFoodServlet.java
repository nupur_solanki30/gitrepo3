package com.HostoPedia.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.Facility;
import com.HostoPedia.bean.Food;
import com.HostoPedia.service.FoodService;
import com.HostoPedia.service.impl.FoodServiceImpl;

/**
 * Servlet implementation class StudentFoodServlet
 */
public class StudentFoodServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	FoodService foodService=new FoodServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentFoodServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<Food> foodList=foodService.fetchFood();
		request.setAttribute("foodList", foodList);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("food_1.jsp");
		requestDispatcher.forward(request, response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
