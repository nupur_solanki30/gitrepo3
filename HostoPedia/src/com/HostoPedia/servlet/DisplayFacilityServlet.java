package com.HostoPedia.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.FacilityService;
import com.HostoPedia.service.impl.FacilityServiceImpl;

//import com.HostoPedia.service.RulesRegulationsService;
//import com.HostoPedia.service.impl.RulesRegulationsServiceImpl;

/**
 * Servlet implementation class DisplayFacilityServlet
 */
public class DisplayFacilityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	FacilityService facilityService = new FacilityServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayFacilityServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ArrayList<com.HostoPedia.bean.Facility> facilityList = facilityService.fetchFacility();
		   System.out.println(facilityList);
	      
		   if(facilityList!=null)
		     {
	        	 RequestDispatcher requestdispatcher=request.getRequestDispatcher("Facilities.jsp");
	        	 request.setAttribute("facilityList", facilityList);
	        
	        	 requestdispatcher.forward(request, response);
		     }
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
