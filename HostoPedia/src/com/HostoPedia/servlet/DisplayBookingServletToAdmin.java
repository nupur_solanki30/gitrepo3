package com.HostoPedia.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.UserService;
import com.HostoPedia.service.impl.UserServiceImpl;

/**
 * Servlet implementation class DisplayBookingServletToAdmin
 */
public class DisplayBookingServletToAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	UserService userservice=new UserServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayBookingServletToAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<com.HostoPedia.bean.Student>studentList=userservice.fetchBookingDetails();
	  	   System.out.println(studentList);
	           if(studentList!=null)
	  	     {
	          	 RequestDispatcher requestdispatcher=request.getRequestDispatcher("DisplayBookingDetailtoAdmin.jsp");
	          	 request.setAttribute("studentList",studentList );
	          	// request.setAttribute("StudentList", studentList);
	          	 requestdispatcher.forward(request, response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
