package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.StaffDetailsService;
import com.HostoPedia.service.impl.StaffDetailsServiceImpl;

/**
 * Servlet implementation class RemoveStaffServlet
 */
public class RemoveStaffServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	StaffDetailsService staffDetailsService=new StaffDetailsServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveStaffServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Remove staff sevlet called");
		String staffId = request.getParameter("staffId");
		
		String removeUserMessage = staffDetailsService.removeStaff(staffId);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayStaffServlet");
		requestDispatcher.forward(request, response);
		
		System.out.println("Remove staff sevlet completed");
	}
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	//}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
