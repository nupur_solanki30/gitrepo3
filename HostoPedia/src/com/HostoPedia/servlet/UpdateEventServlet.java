package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.HostoPedia.bean.Event;
import com.HostoPedia.service.EventService;
import com.HostoPedia.service.impl.EventServiceImpl;



/**
 * Servlet implementation class UpdateEventServlet
 */
public class UpdateEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	EventService eventService=new EventServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateEventServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int eventId = Integer.parseInt(request.getParameter("eventId"));
		System.out.println("Update Event servlet called"+eventId);
		
		Event event = new Event();
		
		Part part = request.getPart("eventImage");
		if(null!=part) {
			System.out.println("Image Name : " + part.getName());
			System.out.println("Image Name 2 : " + part.getSubmittedFileName());
			System.out.println("Image Size :: " + part.getSize());
			event.setEventStream(part.getInputStream());
		}
		String eventName = request.getParameter("eventName");
		String eventDate = request.getParameter("eventDate");
		String Description = request.getParameter("desc");
		
		event.setEventId(eventId);
		event.setEventName(eventName);
		event.setEventDate(eventDate);
		event.setEventDesc(Description);
		
		String eventModifyMessage = eventService.modifyEvent(event);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayEventServlet");
		requestDispatcher.forward(request, response);
	}

}
