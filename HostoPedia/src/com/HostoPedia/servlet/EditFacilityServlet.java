package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.Facility;
import com.HostoPedia.bean.RulesRegulationsBean;
import com.HostoPedia.service.FacilityService;

import com.HostoPedia.service.impl.FacilityServiceImpl;


/**
 * Servlet implementation class EditFacilityServlet
 */
public class EditFacilityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	FacilityService facilityService=new FacilityServiceImpl();   
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditFacilityServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String facilityId = request.getParameter("facilityId");
		System.out.println(facilityId);
		Facility facility = null; 
				
		facility = facilityService.fetchFacilityDetails(facilityId);
		
		facility.setFacilityId(Integer.parseInt(facilityId));
		
		request.setAttribute("facility",facility);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("EditFacility.jsp");
		requestDispatcher.forward(request, response);
	}
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	//}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
