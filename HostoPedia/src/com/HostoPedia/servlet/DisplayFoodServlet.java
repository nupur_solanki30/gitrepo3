package com.HostoPedia.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.FoodService;
import com.HostoPedia.service.impl.FoodServiceImpl;



/**
 * Servlet implementation class DisplayFoodServlet
 */
public class DisplayFoodServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    FoodService foodService = new FoodServiceImpl(); 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayFoodServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 ArrayList<com.HostoPedia.bean.Food> foodList = foodService.fetchFood();
		   System.out.println(foodList);
	      
		   if(foodList!=null)
		     {
	        	 RequestDispatcher requestdispatcher=request.getRequestDispatcher("Food.jsp");
	        	 request.setAttribute("foodList", foodList);
	        
	        	 requestdispatcher.forward(request, response);
		     }
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
