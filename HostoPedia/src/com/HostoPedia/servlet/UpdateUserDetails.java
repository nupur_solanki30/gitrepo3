package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.User;
import com.HostoPedia.service.UserService;
import com.HostoPedia.service.impl.UserServiceImpl;




/**
 * Servlet implementation class UpdateUserDetails
 */
public class UpdateUserDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
	UserService userService = new UserServiceImpl();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateUserDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Update Servlet called");
		String UserName=request.getParameter("UserName");
		String UserEmailid=request.getParameter("UserEmailid");
		String UserContact=request.getParameter("UserContact");
		String UserAdd=request.getParameter("UserAdd");
		String UserPass=request.getParameter("UserPass");
		String UserRole=request.getParameter("UserRole");
		int userId = Integer.parseInt(request.getParameter("userId"));
		System.out.println("User id ========= "+userId);
		
		User user = new User();
		user.setUserName(UserName);
		user.setUserEmailid(UserEmailid);
		user.setUserContact(UserContact);
		user.setUserAdd(UserAdd);
		user.setUserPass(UserPass);
		user.setUserRole(UserRole);
		user.setUserId(userId);
		String userModifyMessage = userService.modifyUserDetails(user);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayAlUserDetails");
		requestDispatcher.forward(request, response);
		//response.sendRedirect("DisplayUserRecords");
		
	//	doGet(request, response);
	}

}