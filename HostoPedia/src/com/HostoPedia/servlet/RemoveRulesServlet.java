package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.RulesRegulationsService;
import com.HostoPedia.service.impl.RulesRegulationsServiceImpl;

/**
 * Servlet implementation class RemoveRulesServlet
 */
public class RemoveRulesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 RulesRegulationsService rulesRegulationsService=new RulesRegulationsServiceImpl();    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveRulesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Remove rules sevlet called");
		String rulesId = request.getParameter("rulesId");
		
		String removeUserMessage = rulesRegulationsService.removeRules(rulesId);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayRulesServlet");
		requestDispatcher.forward(request, response);
		
		System.out.println("Remove rules sevlet completed");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
