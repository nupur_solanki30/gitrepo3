package com.HostoPedia.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.HostoPedia.bean.Feedback;
import com.HostoPedia.bean.User;
import com.HostoPedia.service.FeedbackService;
import com.HostoPedia.service.impl.FeedbackServiceImpl;

/**
 * Servlet implementation class FeedbackServlet
 */
public class FeedbackServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 FeedbackService feedbackService = new FeedbackServiceImpl();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FeedbackServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		System.out.println("Feedback servlet called!");
		HttpSession httpSession=request.getSession(false);
		User user = null;
			if(null!=httpSession)
			{
			 	user=(User) httpSession.getAttribute("loginBean");
			}
			
//		int branchId=user.getBranch();
//		System.out.println("branchId "+branchId);
			
		int userId = user.getUserId();
		System.out.println("userId "+userId);
		
		String rating = request.getParameter("feedback");
		System.out.println(rating);
		String description = request.getParameter("comment");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String dateTime = formatter.format(date);
		System.out.println(dateTime);
		
		Feedback feedBack = new Feedback();
		feedBack.setFeedbackDescription(description);
		feedBack.setFeedbackRating(rating);
		feedBack.setFeedbackDate(dateTime);
		feedBack.setUserId(userId);
		
		String msg = feedbackService.insertFeedbackDetails(feedBack);
		System.out.println(msg);
		response.sendRedirect("Index.jsp");
	}
}
