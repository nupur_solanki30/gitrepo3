package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.HostoPedia.bean.User;
import com.HostoPedia.service.UserService;
import com.HostoPedia.service.impl.UserServiceImpl;
import com.HostoPedia.bean.Student;


/**
 * Servlet implementation class UpdateStudentDetails
 */
public class UpdateStudentDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
	UserService userService = new UserServiceImpl();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateStudentDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		System.out.println("Update student Servlet called");
		Student student = new Student();
		User user = new User();
		String username = request.getParameter("username");
		String contact = request.getParameter("usercontact");
		String useradd=request.getParameter("useradd");
		String userpass=request.getParameter("userpass");
		String userrole=request.getParameter("userrole");
		String StuClgName=request.getParameter("StuClgName");
		String email=request.getParameter("email");
		Part part = request.getPart("studentDocument");
		if(part!=null && part.getSize()>0) {
			student.setStuDocStream(part.getInputStream());
		}
	//	String StuDocString=request.getParameter("StuDocString");
		String StuDob=request.getParameter("StuDob");
		String StuFatherName=request.getParameter("StuFatherName");
		String StuMotherName=request.getParameter("StuMotherName");
		String UserId=request.getParameter("UserId");
		System.out.println("User id ========= "+UserId);
		
		int StuId = Integer.parseInt(request.getParameter("StuId"));
		
		
		student.setStuClgName(StuClgName);
	//	student.setStuDocString(StuDocString);
		student.setStuDob(StuDob);
		student.setStuFatherName(StuFatherName);
		student.setStuMotherName(StuMotherName);
		student.setStuId(StuId);
		student.setUserContact(contact);
		student.setUserName(username);
		student.setUserEmailid(email);
		student.setUserAdd(useradd);
		student.setUserPass(userpass);
		student.setUserRole(userrole);
		
		
		student.setUserId(Integer.parseInt(UserId));
		String studentModifyMessage = userService.modifyStudentDetails(user,student);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayAlUserDetails");
		requestDispatcher.forward(request, response);
		
		//doGet(request, response);
	}

}
