package com.HostoPedia.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.Facility;
import com.HostoPedia.bean.RulesRegulationsBean;
import com.HostoPedia.service.RulesRegulationsService;
import com.HostoPedia.service.impl.RulesRegulationsServiceImpl;

/**
 * Servlet implementation class StudentRulesRegulationServlet
 */
public class StudentRulesRegulationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	RulesRegulationsService rulesRegulationsService=new RulesRegulationsServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentRulesRegulationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<RulesRegulationsBean> rulesregulationList=rulesRegulationsService.fetchRules();
		request.setAttribute("rulesregulationList", rulesregulationList);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("rulesRegulation_1.jsp");
		requestDispatcher.forward(request, response);
		//	response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
