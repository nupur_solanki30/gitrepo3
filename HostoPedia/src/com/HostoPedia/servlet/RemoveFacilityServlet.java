package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.FacilityService;
import com.HostoPedia.service.impl.FacilityServiceImpl;

/**
 * Servlet implementation class RemoveFacilityServlet
 */
public class RemoveFacilityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	FacilityService facilityService=new FacilityServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveFacilityServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Remove facility sevlet called");
		String facilityId = request.getParameter("facilityId");
		
		String removeFacilityMessage = facilityService.removeFacility(facilityId);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayFacilityServlet");
		requestDispatcher.forward(request, response);
		
		System.out.println("Remove rules sevlet completed");
	}
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	//}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
