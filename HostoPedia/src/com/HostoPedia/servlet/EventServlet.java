package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.HostoPedia.bean.Event;
import com.HostoPedia.service.EventService;
import com.HostoPedia.service.impl.EventServiceImpl;

/**
 * Servlet implementation class EventServlet
 */
public class EventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	EventService eventService=new EventServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EventServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Event Servlet called");
		
		Part part = request.getPart("eventImg");
		
		Event event = new Event();
		if(null!=part) {
			System.out.println("Image Name : " + part.getName());
			System.out.println("Image Name 2 : " + part.getSubmittedFileName());
			System.out.println("Image Size :: " + part.getSize());
			event.setEventStream(part.getInputStream());
		}
		String eventName = request.getParameter("eventName");
		String eventDate = request.getParameter("eventDate");
		String description = request.getParameter("description");
		
		System.out.println(description);
		System.out.println(eventName);
		
		event.setEventName(eventName);
		event.setEventDate(eventDate);
		event.setEventDesc(description);
		
		String eventMessage = eventService.saveEventDetails(event);
		System.out.println("Servlet execution complete");
		response.getWriter().append(eventMessage);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayEventServlet");	
		requestDispatcher.forward(request, response);
	}
}
