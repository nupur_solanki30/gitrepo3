package com.HostoPedia.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.GalleryService;
import com.HostoPedia.service.impl.GalleryServiceImpl;

/**
 * Servlet implementation class DisplayGalleryServlet
 */
public class DisplayGalleryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    GalleryService galleryService = new GalleryServiceImpl();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayGalleryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ArrayList<com.HostoPedia.bean.Gallery> galleryList = galleryService.fetchGallery();
		   System.out.println(galleryList);
	      
		   if(galleryList!=null)
		     {
	        	 RequestDispatcher requestdispatcher=request.getRequestDispatcher("Gallery.jsp");
	        	 request.setAttribute("galleryList", galleryList);
	        
	        	 requestdispatcher.forward(request, response);
		     }
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
