package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.RulesRegulationsBean;
import com.HostoPedia.service.RulesRegulationsService;
import com.HostoPedia.service.impl.RulesRegulationsServiceImpl;

/**
 * Servlet implementation class RulesRegulationsServlet
 */
public class RulesRegulationsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       RulesRegulationsService rulesRegulationsService = new RulesRegulationsServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RulesRegulationsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Rules Servlet called");
		
		String description = request.getParameter("Desc");
		String applyDate = request.getParameter("Date");
		
		System.out.println(description);
		System.out.println(applyDate);
		
		RulesRegulationsBean rulesRegulationsBean = new RulesRegulationsBean();
		
		rulesRegulationsBean.setDescription(description);
		rulesRegulationsBean.setApplyDate(applyDate);

		String rulesMessage = rulesRegulationsService.saveRulesDetails(rulesRegulationsBean);
		System.out.println("Servlet execution complete");
		response.getWriter().append(rulesMessage);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayRulesServlet");	
		requestDispatcher.forward(request, response);

	}
}
