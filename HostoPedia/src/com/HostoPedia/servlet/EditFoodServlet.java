package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.Food;
import com.HostoPedia.service.FoodService;
import com.HostoPedia.service.impl.FoodServiceImpl;

/**
 * Servlet implementation class EditFoodServlet
 */
public class EditFoodServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      FoodService foodService = new FoodServiceImpl(); 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditFoodServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String foodId = request.getParameter("foodId");
		System.out.println(foodId);
		Food food = null; 
				
		food = foodService.fetchFood(foodId);
		
		food.setFoodId(Integer.parseInt(foodId));
		
		request.setAttribute("food", food);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("EditFood.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
