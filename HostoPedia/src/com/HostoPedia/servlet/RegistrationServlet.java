package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.HostoPedia.bean.EmailThread;
import com.HostoPedia.bean.Student;
import com.HostoPedia.bean.User;
import com.HostoPedia.service.RegistrationService;
import com.HostoPedia.service.impl.RegistrationServiceImpl;

/**
 * Servlet implementation class RegistrationServlet
 */
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	RegistrationService registrationService = new RegistrationServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistrationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Registation servlet called");
		// String UserId=request.getParameter("UserId");
		String userName = request.getParameter("fullname");
		String userEmailid = request.getParameter("email");
		String userContact = request.getParameter("contactno");
		String userAdd = request.getParameter("address");
		String userPass = request.getParameter("txtPassword");
		String userRole = request.getParameter("role");
		String collegeName = request.getParameter("collegename");
		String fatherName = request.getParameter("fathername");
		String motherName = request.getParameter("mothername");
		String dob = request.getParameter("dob");
		Part part = request.getPart("doc");
		
		Student student = new Student();
		if(null!=part) {
			System.out.println("File Name" + part.getName());
			System.out.println("File Name 2" + part.getSubmittedFileName());
			System.out.println("File Size :: " + part.getSize());
			student.setStuDocStream( part.getInputStream());
		}
		// System.out.println(UserId);
		System.out.println(userName);
		System.out.println(userEmailid);
		System.out.println(userContact);
		System.out.println(userAdd);
		System.out.println(userPass);
		System.out.println(userRole);
		System.out.println(collegeName);
		System.out.println(fatherName);
		System.out.println(motherName);
		System.out.println(dob);
		
		User user = new User();

		user.setUserName(userName);
		user.setUserEmailid(userEmailid);
		user.setUserContact(userContact);
		user.setUserAdd(userAdd);
		user.setUserPass(userPass);
		user.setUserRole(userRole);
		
		student.setStuClgName(collegeName);
		student.setStuDob(dob);
		student.setStuFatherName(fatherName);
		student.setStuMotherName(motherName);

		String registrationMessage = registrationService.saveStudentDetails(user, student);
		System.out.println("Servlet execution complete");
		response.getWriter().append(registrationMessage);
		
		try 
		{
			String msg= "Welcome "+userName+" your registration is successfull in HostoPedia.";
			
			EmailThread thread = new EmailThread();
			thread.send(userEmailid, msg);
			Thread t1 = new Thread(thread);
			t1.start();
			
		}
		catch (Exception e) {
			System.out.println("Mail sending failed");
		}
	
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("Login.jsp");	
			requestDispatcher.forward(request, response);	
		}
	}


