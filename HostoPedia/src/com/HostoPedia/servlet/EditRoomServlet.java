package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.Room;
import com.HostoPedia.service.RoomService;
import com.HostoPedia.service.impl.RoomServiceImpl;

/**
 * Servlet implementation class EditRoomServlet
 */
public class EditRoomServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    RoomService roomService = new RoomServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditRoomServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String roomId = request.getParameter("roomId");
		System.out.println(roomId);
		Room room = null; 
				
		room = roomService.fetchRoomDetails(roomId);
		
		room.setRoomId(Integer.parseInt(roomId));
		
		request.setAttribute("room",room);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("EditRoom.jsp");
		requestDispatcher.forward(request, response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
