package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.UserService;
import com.HostoPedia.service.impl.UserServiceImpl;

/**
 * Servlet implementation class ResetPassword
 */
public class ResetPassword extends HttpServlet {
	private static final long serialVersionUID = 1L;
	UserService userService = new UserServiceImpl(); 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResetPassword() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email=request.getParameter("email");
		String password=request.getParameter("password");
		
		String msg =userService.modifyPassword(password, email);
		System.out.println("msg");
		if(msg.equals("Password Updated Sucessfully"))
		{
			RequestDispatcher requestDispatcher=request.getRequestDispatcher("Forgetpassword.jsp");
			requestDispatcher.forward(request, response);
			
		}
		else
		{
			RequestDispatcher requestDispatcher=request.getRequestDispatcher("Login.jsp");
			requestDispatcher.forward(request, response);
		}
	}
}
