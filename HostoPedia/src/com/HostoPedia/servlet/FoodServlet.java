package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.bean.Food;
import com.HostoPedia.bean.RulesRegulationsBean;
import com.HostoPedia.service.FoodService;
import com.HostoPedia.service.impl.FoodServiceImpl;


/**
 * Servlet implementation class FoodServlet
 */
public class FoodServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 FoodService foodService = new FoodServiceImpl();
  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FoodServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Food Servlet called");
		
		String foodName = request.getParameter("foodName");
		String foodType = request.getParameter("foodType");
		String day = request.getParameter("foodDay");
		
		System.out.println(foodName);
		System.out.println(foodType);
		System.out.println(day);
		
		Food food = new Food();
		food.setFoodName(foodName);
		food.setFoodType(foodType);
		food.setDay(day);

		String foodMessage = foodService.saveFood(food);
		System.out.println("Servlet execution complete");
		response.getWriter().append(foodMessage);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayFoodServlet");	
		requestDispatcher.forward(request, response);
	}
}
