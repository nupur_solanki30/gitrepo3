package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.HostoPedia.bean.Event;
import com.HostoPedia.bean.Gallery;
import com.HostoPedia.service.GalleryService;
import com.HostoPedia.service.impl.GalleryServiceImpl;

/**
 * Servlet implementation class UpdateGalleryServlet
 */
public class UpdateGalleryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    GalleryService galleryService = new GalleryServiceImpl();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateGalleryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		int galleryId = Integer.parseInt(request.getParameter("galleryId"));
		System.out.println("Update gallery servlet called "+galleryId);
		
		Gallery gallery = new Gallery();
		
		Part part = request.getPart("galleryImage");
		if(null!=part) {
			System.out.println("Image Name : " + part.getName());
			System.out.println("Image Name 2 : " + part.getSubmittedFileName());
			System.out.println("Image Size :: " + part.getSize());
			gallery.setGalleryStream(part.getInputStream());
		}

		gallery.setGalleryId(galleryId);
		
		String galleryModifyMessage = galleryService.modifyGallery(gallery);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayGalleryServlet");
		requestDispatcher.forward(request, response);
		
		//doGet(request, response);
	}

}
