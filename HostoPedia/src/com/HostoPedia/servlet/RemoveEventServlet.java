package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.EventService;
import com.HostoPedia.service.impl.EventServiceImpl;

/**
 * Servlet implementation class RemoveEventServlet
 */
public class RemoveEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	EventService eventService = new EventServiceImpl();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveEventServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Remove event servlet called");
		String eventId = request.getParameter("eventId");
		
		String removeEventMessage = eventService.removeEvent(eventId);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayEventServlet");
		requestDispatcher.forward(request, response);
		
		System.out.println("Remove event sevlet completed");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
