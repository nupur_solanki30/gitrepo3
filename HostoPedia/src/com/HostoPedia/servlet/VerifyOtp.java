package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class VerifyOtp
 */
public class VerifyOtp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VerifyOtp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String original=request.getParameter("verifyOtp");
		String  validate=request.getParameter("otp");
		String email=request.getParameter("email");
		System.out.println("Original : "+original);
		System.out.println("Validate : "+validate);
		System.out.println("Email : "+email);
		if(original.equals(validate))
		{
			System.out.println("valid otp");
			request.setAttribute("email", email);
			RequestDispatcher requestDispatcher=request.getRequestDispatcher("ResetPass.jsp");
			requestDispatcher.forward(request, response);
		}
		else
		{
			System.out.println("Invalid Otp");
			RequestDispatcher requestDispatcher=request.getRequestDispatcher("ForgetPassword.jsp");
			requestDispatcher.forward(request, response);
		}
	}

}
