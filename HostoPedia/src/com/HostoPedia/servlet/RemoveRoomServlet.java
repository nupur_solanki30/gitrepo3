package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.RoomService;
import com.HostoPedia.service.impl.RoomServiceImpl;

/**
 * Servlet implementation class RemoveRoomServlet
 */
public class RemoveRoomServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	RoomService roomService = new RoomServiceImpl();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveRoomServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Remove room servlet called");
		String roomId = request.getParameter("roomId");
		
		String removeRoomMessage = roomService.removeRoom(roomId);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayRoomServlet");
		requestDispatcher.forward(request, response);
		
		System.out.println("Remove room sevlet completed");
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
