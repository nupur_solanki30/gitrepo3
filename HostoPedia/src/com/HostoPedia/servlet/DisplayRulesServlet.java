package com.HostoPedia.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.RulesRegulationsService;
import com.HostoPedia.service.impl.RulesRegulationsServiceImpl;

/**
 * Servlet implementation class DisplayRulesServlet
 */
public class DisplayRulesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    RulesRegulationsService rulesRegulationsService = new RulesRegulationsServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayRulesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 ArrayList<com.HostoPedia.bean.RulesRegulationsBean> rulesList = rulesRegulationsService.fetchRules();
		   System.out.println(rulesList);
	      
		   if(rulesList!=null)
		     {
	        	 RequestDispatcher requestdispatcher=request.getRequestDispatcher("RulesRegulations.jsp");
	        	 request.setAttribute("rulesList", rulesList);
	        
	        	 requestdispatcher.forward(request, response);
		     }
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
