package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.HostoPedia.service.GalleryService;
import com.HostoPedia.service.impl.GalleryServiceImpl;

/**
 * Servlet implementation class RemoveGalleryServlet
 */
public class RemoveGalleryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    GalleryService galleryService = new GalleryServiceImpl();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveGalleryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Remove gallery servlet called");
		String galleryId = request.getParameter("galleryId");
		
		String removeGalleryMessage = galleryService.removeGallery(galleryId);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("DisplayGalleryServlet");
		requestDispatcher.forward(request, response);
		
		System.out.println("Remove gallery sevlet completed");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
