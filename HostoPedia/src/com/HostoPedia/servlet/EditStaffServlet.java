package com.HostoPedia.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.HostoPedia.bean.StaffDetails;
import com.HostoPedia.service.StaffDetailsService;
import com.HostoPedia.service.impl.StaffDetailsServiceImpl;

/**
 * Servlet implementation class EditStaffServlet
 */
public class EditStaffServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	StaffDetailsService staffDetailsService=new StaffDetailsServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditStaffServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String staffId = request.getParameter("staffId");
		String gender = request.getParameter("Gender");
		
		System.out.println(gender);
		System.out.println(staffId);
		StaffDetails staffDetails = null; 
				
		staffDetails = staffDetailsService.fetchStaffDetails(staffId);
				//fetchRulesDetails(rulesId);
		
		staffDetails.setStaffId(Integer.parseInt(staffId));
		staffDetails.setGender(gender);
		
		request.setAttribute("staffDetails", staffDetails);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("EditStaff.jsp");
		requestDispatcher.forward(request, response);
	}

		//response.getWriter().append("Served at: ").append(request.getContextPath());
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
