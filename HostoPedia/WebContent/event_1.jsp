<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.HostoPedia.bean.Event"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
<% List <Event> eventList = (List) request.getAttribute("eventList"); %>
<head>
<meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Events</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

</head>
<body>
 <section id="events" class="events">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Events</h2>
          <p>Some Event Organized by Our Hostel</p>
        </div>

        <div class="events-slider swiper-container" data-aos="fade-up" data-aos-delay="100">
          <div class="swiper-wrapper">
<%-- 			<% for(Event event : eventList) { %> --%>
				<% for (int i=0; i<eventList.size();i=i+3) {%>
            <div class="swiper-slide">
              <div class="row event-item">
              	<%if(eventList.get(i)!=null){ %>
                <div class="col-lg-6">
               
                  <img src="data:image/jpeg;base64,<%= eventList.get(i).getEventString() %>" height="500" width="500" class="img-fluid" alt="">
                </div>
                <div class="col-lg-6 pt-4 pt-lg-0 content">
                  <h3><%=eventList.get(i).getEventName() %></h3>
<!--                   <div class="price"> -->
<!--                     <p><span>$189</span></p> -->
<!--                   </div> -->
                  <p class="font-italic">
                    <%=eventList.get(i).getEventDesc() %> <br>
                    This event is held on <%=eventList.get(i).getEventDate() %>
                  </p>
<!--                   <ul> -->
<!--                     <li><i class="bi bi-check-circled"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li> -->
<!--                     <li><i class="bi bi-check-circled"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li> -->
<!--                     <li><i class="bi bi-check-circled"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li> -->
<!--                   </ul> -->
<!--                   <p> -->
<!--                     Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate -->
<!--                     velit esse cillum dolore eu fugiat nulla pariatur -->
<!--                   </p> -->
                </div>
              </div>
            </div><!-- End testimonial item -->
			<%} %>
            <div class="swiper-slide">
              <div class="row event-item">
              <% if( (i+1)<eventList.size() && eventList.get(i+1)!=null){ %>
                <div class="col-lg-6">
                  <img src="data:image/jpeg;base64,<%= eventList.get(i+1).getEventString() %>" height="500" width="500" class="img-fluid" alt="">
                </div>
                <div class="col-lg-6 pt-4 pt-lg-0 content">
                  <h3><%=eventList.get(i+1).getEventName() %></h3>
<!--                   <div class="price"> -->
<!--                     <p><span>$290</span></p> -->
<!--                   </div> -->
                  <p class="font-italic">
                    <%=eventList.get(i+1).getEventDesc() %> <br>
                    This event is held on <%=eventList.get(i+1).getEventDate() %>
                  </p>
<!--                   <ul> -->
<!--                     <li><i class="bi bi-check-circled"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li> -->
<!--                     <li><i class="bi bi-check-circled"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li> -->
<!--                     <li><i class="bi bi-check-circled"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li> -->
<!--                   </ul> -->
<!--                   <p> -->
<!--                     Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate -->
<!--                     velit esse cillum dolore eu fugiat nulla pariatur -->
<!--                   </p> -->
                </div>
                <%} %>
              </div>
            </div><!-- End testimonial item -->

            <div class="swiper-slide">
              <div class="row event-item">
              <% if( (i+2)<eventList.size() && eventList.get(i+2)!=null){ %>
                <div class="col-lg-6">
                  <img src="data:image/jpeg;base64,<%= eventList.get(i+2).getEventString() %>" height="500" width="500" class="img-fluid" alt="">
                </div>
                <div class="col-lg-6 pt-4 pt-lg-0 content">
                  <h3><%=eventList.get(i+2).getEventName() %></h3>
<!--                   <div class="price"> -->
<!--                     <p><span>$99</span></p> -->
<!--                   </div> -->
                  <p class="font-italic">
                   	<%=eventList.get(i+2).getEventDesc() %> <br>
                   	This event is held on <%=eventList.get(i+2).getEventDate() %>
                  </p>
<!--                   <ul> -->
<!--                     <li><i class="bi bi-check-circled"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li> -->
<!--                     <li><i class="bi bi-check-circled"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li> -->
<!--                     <li><i class="bi bi-check-circled"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li> -->
<!--                   </ul> -->
<!--                   <p> -->
<!--                     Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate -->
<!--                     velit esse cillum dolore eu fugiat nulla pariatur -->
<!--                   </p> -->
                </div>
                <%} %>
               <%} %>
              </div>
            </div><!-- End testimonial item -->

          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
    </section>
    
  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
    
</body>
</html>