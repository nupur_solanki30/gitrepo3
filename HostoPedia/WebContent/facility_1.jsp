<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.HostoPedia.bean.Facility"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
<% List <Facility> facilityList = (List) request.getAttribute("facilityList"); %>
<head>
<meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Facility</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>
<body>
<section id="facility" class="facility">

      <div class="container" data-aos="fade-up">
        <div class="section-title">
          <h2>Facility</h2>
          <p>Some facilities from Our Hostel</p>
        </div>
      </div>

      <div class="container-fluid" data-aos="fade-up" data-aos-delay="100">
<%-- 		<% for(Facility facility : facilityList) { %> --%>
			<% for (int i=0; i<facilityList.size();i=i+4) {%>
        <div class="row g-0">
        <%if(facilityList.get(i)!=null){ %>
          <div class="col-lg-3 col-md-4">
            <div class="event-item">
<%--                  <a href="data:image/jpeg;base64,<%= facility.getFacilityString() %>" class="facility-lightbox" data-gall="facility-item"> --%>
                <img src="data:image/jpeg;base64,<%= facilityList.get(i).getFacilityString() %>" alt="" class="img-fluid">
<!--                	</a> -->
            	
            	<br><%=facilityList.get(i).getDescription() %>
                <br><%=facilityList.get(i).getFacilityName() %><br>
             
            </div>
          </div>
          <%} %>
          
          <% if( (i+1)<facilityList.size() && facilityList.get(i+1)!=null){ %>
          	<div class="col-lg-3 col-md-4">
            <div class="event-item">
<%--                  <a href="data:image/jpeg;base64,<%= facility.getFacilityString() %>" class="facility-lightbox" data-gall="facility-item"> --%>
                <img src="data:image/jpeg;base64,<%= facilityList.get(i+1).getFacilityString() %>" alt="" class="img-fluid">
<!--                	</a> -->
            	
            	<br><%=facilityList.get(i+1).getDescription() %>
                <br><%=facilityList.get(i+1).getFacilityName() %><br>
             
            </div>
          </div>
          <%} %>
          
          <% if( (i+2)<facilityList.size() && facilityList.get(i+1)!=null){ %>
          	<div class="col-lg-3 col-md-4">
            <div class="event-item">
<%--                  <a href="data:image/jpeg;base64,<%= facility.getFacilityString() %>" class="facility-lightbox" data-gall="facility-item"> --%>
                <img src="data:image/jpeg;base64,<%= facilityList.get(i+2).getFacilityString() %>" alt="" class="img-fluid">
<!--                	</a> -->
            	
            	<br><%=facilityList.get(i+2).getDescription() %>
                <br><%=facilityList.get(i+2).getFacilityName() %><br>
             
            </div>
          </div>
          <%} %>
          
          <% if( (i+3)<facilityList.size() && facilityList.get(i+1)!=null){ %>
          	<div class="col-lg-3 col-md-4">
            <div class="event-item">
<%--                  <a href="data:image/jpeg;base64,<%= facility.getFacilityString() %>" class="facility-lightbox" data-gall="facility-item"> --%>
                <img src="data:image/jpeg;base64,<%= facilityList.get(i+3).getFacilityString() %>" alt="" class="img-fluid">
<!--                	</a> -->
            	
            	<br><%=facilityList.get(i+3).getDescription() %>
                <br><%=facilityList.get(i+3).getFacilityName() %><br>
             
            </div>
          </div>
          <%} %>
          
         <%} %>

<!--           <div class="col-lg-3 col-md-4"> -->
<!--             <div class="gallery-item"> -->
<!--               <a href="assets/img/gallery/gallery-2.jpg" class="gallery-lightbox" data-gall="gallery-item"> -->
<!--                 <img src="assets/img/gallery/gallery-2.jpg" alt="" class="img-fluid"> -->
<!--               </a> -->
<!--             </div> -->
<!--           </div> -->

<!--           <div class="col-lg-3 col-md-4"> -->
<!--             <div class="gallery-item"> -->
<!--               <a href="assets/img/gallery/gallery-3.jpg" class="gallery-lightbox" data-gall="gallery-item"> -->
<!--                 <img src="assets/img/gallery/gallery-3.jpg" alt="" class="img-fluid"> -->
<!--               </a> -->
<!--             </div> -->
<!--           </div> -->

<!--           <div class="col-lg-3 col-md-4"> -->
<!--             <div class="gallery-item"> -->
<!--               <a href="assets/img/gallery/gallery-4.jpg" class="gallery-lightbox" data-gall="gallery-item"> -->
<!--                 <img src="assets/img/gallery/gallery-4.jpg" alt="" class="img-fluid"> -->
<!--               </a> -->
<!--             </div> -->
<!--           </div> -->

<!--           <div class="col-lg-3 col-md-4"> -->
<!--             <div class="gallery-item"> -->
<!--               <a href="assets/img/gallery/gallery-5.jpg" class="gallery-lightbox" data-gall="gallery-item"> -->
<!--                 <img src="assets/img/gallery/gallery-5.jpg" alt="" class="img-fluid"> -->
<!--               </a> -->
<!--             </div> -->
<!--           </div> -->

<!--           <div class="col-lg-3 col-md-4"> -->
<!--             <div class="gallery-item"> -->
<!--               <a href="assets/img/gallery/gallery-6.jpg" class="gallery-lightbox" data-gall="gallery-item"> -->
<!--                 <img src="assets/img/gallery/gallery-6.jpg" alt="" class="img-fluid"> -->
<!--               </a> -->
<!--             </div> -->
<!--           </div> -->

<!--           <div class="col-lg-3 col-md-4"> -->
<!--             <div class="gallery-item"> -->
<!--               <a href="assets/img/gallery/gallery-7.jpg" class="gallery-lightbox" data-gall="gallery-item"> -->
<!--                 <img src="assets/img/gallery/gallery-7.jpg" alt="" class="img-fluid"> -->
<!--               </a> -->
<!--             </div> -->
<!--           </div> -->

<!--           <div class="col-lg-3 col-md-4"> -->
<!--             <div class="gallery-item"> -->
<!--               <a href="assets/img/gallery/gallery-8.jpg" class="gallery-lightbox" data-gall="gallery-item"> -->
<!--                 <img src="assets/img/gallery/gallery-8.jpg" alt="" class="img-fluid"> -->
<!--               </a> -->
<!--             </div> -->
<!--           </div> -->

        </div>
      </div>
    </section>
    
  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>
</html>