<%@page import="com.HostoPedia.bean.PaytmConstants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,com.paytm.pg.merchant.CheckSumServiceHelper"%>
<%@ page session="false" %>

<%
Enumeration<String> paramNames = request.getParameterNames();

Map<String, String[]> mapData = request.getParameterMap();
TreeMap<String,String> parameters = new TreeMap<String,String>();
String paytmChecksum =  "";
while(paramNames.hasMoreElements()) {
	String paramName = (String)paramNames.nextElement();
	if(paramName.equals("CHECKSUMHASH")){
		paytmChecksum = mapData.get(paramName)[0];
	}else{
		parameters.put(paramName,mapData.get(paramName)[0]);
	}
}
boolean isValideChecksum = false;
String outputHTML="";
try{
	isValideChecksum = CheckSumServiceHelper.getCheckSumServiceHelper().verifycheckSum(PaytmConstants.MERCHANT_KEY,parameters,paytmChecksum);
	if(isValideChecksum && parameters.containsKey("RESPCODE")){
		if(parameters.get("RESPCODE").equals("01")){
			outputHTML = parameters.toString();
		}else{
			outputHTML="<b>Payment Failed.</b>";
		}
	}else{
		outputHTML="<b>Checksum mismatched.</b>";
	}
}catch(Exception e){
	outputHTML=e.toString();
}
%>
<%
String paymentId=parameters.get("PAYMENTID");
String ammount=parameters.get("TXNAMOUNT");
double Ammount=Double.parseDouble(ammount);
String date=parameters.get("TXNDATE"); 
String status=parameters.get("RESPCODE"); 
int Status=Integer.parseInt(status); 
String orderID = parameters.get("ORDERID");

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<body>
<%= outputHTML %>
</body>
<meta name="GENERATOR" content="Evrsoft First Page">
<script src= "assets/js/jquery-3.5.1.js"></script>
<script type="text/javascript">
$( document ).ready(function() {
    console.log( "ready!" );
<%--     $.post( "DisplayBookingDetailsServlet", { name: "<%=outputHTML%>",paymentId:"<%=paymentId%>",date:"<%=date%>" ,status:"<%=Status%>"}) --%>
//     .done(function( data ) {
//       alert( "Data Loaded: " + data );
//     });
    var orderId = "<%=orderID%>";
    var status = "<%=parameters.get("STATUS")%>";
	$.post( "OrderConfirmationServlet", { ORDERID: orderId ,status: status})
	.done(function( data ) {
	  
	});
    
});
</script>

</html>