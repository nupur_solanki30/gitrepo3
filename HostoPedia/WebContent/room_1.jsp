<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.HostoPedia.bean.Room"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
<% List <Room> roomList = (List) request.getAttribute("roomList"); %>
<head>
<meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Room</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>
<body>
<section id="room" class="room">

      <div class="container" data-aos="fade-up">
        <div class="section-title">
          <h2>Room</h2>
          <p>Some photos from Our hostel rooms.</p>
        </div>
      </div>

      <div class="container-fluid" data-aos="fade-up" data-aos-delay="100">
<%-- 		<% for(Room room : roomList) { %> --%>
			<% for (int i=0; i<roomList.size();i=i+4) {%>
        <div class="row g-0">
        <%if(roomList.get(i)!=null){ %>
          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
<%--                  <a href="data:image/jpeg;base64,<%= room.getRoomString() %>" class="room-lightbox" data-gall="room-item"> --%>
                <img src="data:image/jpeg;base64,<%= roomList.get(i).getRoomString() %>" alt="" class="img-fluid">
<!--                 </a>  -->
			<br><%=roomList.get(i).getRoomDesc() %>
            <br> Room Capacity: <%=roomList.get(i).getRoomCape() %>
            <br> Floor No.: <%=roomList.get(i).getFloorNo() %>
            <br> Room No.: <%=roomList.get(i).getRoomNo() %>
            <br> Room Available.: <%=roomList.get(i).getRoomAvailabilityStatus() %>
            <%if("Availalbe".equalsIgnoreCase(roomList.get(i).getRoomAvailabilityStatus())){ %>
            	<br><a href = "BookRoom?roomId=<%=roomList.get(i).getRoomId() %>">Book Room</a><% }%>
            </div>
          </div>
          <%} %>
          
          <% if( (i+1)<roomList.size() && roomList.get(i+1)!=null){ %>
          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
<%--                  <a href="data:image/jpeg;base64,<%= room.getRoomString() %>" class="room-lightbox" data-gall="room-item"> --%>
                <img src="data:image/jpeg;base64,<%= roomList.get(i+1).getRoomString() %>" alt="" class="img-fluid">
<!--                 </a>  -->
			<br><%=roomList.get(i+1).getRoomDesc() %>
            <br> Room Capacity: <%=roomList.get(i+1).getRoomCape() %>
            <br> Floor No.: <%=roomList.get(i+1).getFloorNo() %>
            <br> Room No.: <%=roomList.get(i+1).getRoomNo() %>
            <br> Room Available.: <%=roomList.get(i+1).getRoomAvailabilityStatus() %>
            <%if("Availalbe".equalsIgnoreCase(roomList.get(i+1).getRoomAvailabilityStatus())){ %>
            	<br><a href = "BookRoom?roomId=<%=roomList.get(i+1).getRoomId() %>">Book Room</a><% }%>
            </div>
          </div>
          <%} %>
          
          <% if( (i+2)<roomList.size() && roomList.get(i+1)!=null){ %>
          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
<%--                  <a href="data:image/jpeg;base64,<%= room.getRoomString() %>" class="room-lightbox" data-gall="room-item"> --%>
                <img src="data:image/jpeg;base64,<%= roomList.get(i+2).getRoomString() %>" alt="" class="img-fluid">
<!--                 </a>  -->
			<br><%=roomList.get(i+2).getRoomDesc() %>
            <br> Room Capacity: <%=roomList.get(i+2).getRoomCape() %>
            <br> Floor No.: <%=roomList.get(i+2).getFloorNo() %>
            <br> Room No.: <%=roomList.get(i+2).getRoomNo() %>
            <br> Room Available.: <%=roomList.get(i+2).getRoomAvailabilityStatus() %>
            	<%if("Availalbe".equalsIgnoreCase(roomList.get(i+2).getRoomAvailabilityStatus())){ %>
            	<br><a href = "BookRoom?roomId=<%=roomList.get(i+2).getRoomId() %>">Book Room</a><% }%>
            </div>
          </div>
          <%} %>
          
          <% if( (i+3)<roomList.size() && roomList.get(i+1)!=null){ %>
          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
<%--                  <a href="data:image/jpeg;base64,<%= room.getRoomString() %>" class="room-lightbox" data-gall="room-item"> --%>
                <img src="data:image/jpeg;base64,<%= roomList.get(i+3).getRoomString() %>" alt="" class="img-fluid">
<!--                 </a>  -->
			<br><%=roomList.get(i+3).getRoomDesc() %>
            <br> Room Capacity: <%=roomList.get(i+3).getRoomCape() %>
            <br> Floor No.: <%=roomList.get(i+3).getFloorNo() %>
            <br> Room No.: <%=roomList.get(i+3).getRoomNo() %>
            <br> Room Available.: <%=roomList.get(i+3).getRoomAvailabilityStatus() %>
            <%if("Availalbe".equalsIgnoreCase(roomList.get(i+3).getRoomAvailabilityStatus())){ %>
            	<br><a href = "BookRoom?roomId=<%=roomList.get(i+3).getRoomId() %>">Book Room</a><% }%>
            </div>
          </div>
          <%} %>
          
         <%} %>
          

<!--           <div class="col-lg-3 col-md-4"> -->
<!--             <div class="gallery-item"> -->
<!--               <a href="assets/img/gallery/gallery-2.jpg" class="gallery-lightbox" data-gall="gallery-item"> -->
<!--                 <img src="assets/img/gallery/gallery-2.jpg" alt="" class="img-fluid"> -->
<!--               </a> -->
<!--             </div> -->
<!--           </div> -->

<!--           <div class="col-lg-3 col-md-4"> -->
<!--             <div class="gallery-item"> -->
<!--               <a href="assets/img/gallery/gallery-3.jpg" class="gallery-lightbox" data-gall="gallery-item"> -->
<!--                 <img src="assets/img/gallery/gallery-3.jpg" alt="" class="img-fluid"> -->
<!--               </a> -->
<!--             </div> -->
<!--           </div> -->

<!--           <div class="col-lg-3 col-md-4"> -->
<!--             <div class="gallery-item"> -->
<!--               <a href="assets/img/gallery/gallery-4.jpg" class="gallery-lightbox" data-gall="gallery-item"> -->
<!--                 <img src="assets/img/gallery/gallery-4.jpg" alt="" class="img-fluid"> -->
<!--               </a> -->
<!--             </div> -->
<!--           </div> -->

<!--           <div class="col-lg-3 col-md-4"> -->
<!--             <div class="gallery-item"> -->
<!--               <a href="assets/img/gallery/gallery-5.jpg" class="gallery-lightbox" data-gall="gallery-item"> -->
<!--                 <img src="assets/img/gallery/gallery-5.jpg" alt="" class="img-fluid"> -->
<!--               </a> -->
<!--             </div> -->
<!--           </div> -->

<!--           <div class="col-lg-3 col-md-4"> -->
<!--             <div class="gallery-item"> -->
<!--               <a href="assets/img/gallery/gallery-6.jpg" class="gallery-lightbox" data-gall="gallery-item"> -->
<!--                 <img src="assets/img/gallery/gallery-6.jpg" alt="" class="img-fluid"> -->
<!--               </a> -->
<!--             </div> -->
<!--           </div> -->

<!--           <div class="col-lg-3 col-md-4"> -->
<!--             <div class="gallery-item"> -->
<!--               <a href="assets/img/gallery/gallery-7.jpg" class="gallery-lightbox" data-gall="gallery-item"> -->
<!--                 <img src="assets/img/gallery/gallery-7.jpg" alt="" class="img-fluid"> -->
<!--               </a> -->
<!--             </div> -->
<!--           </div> -->

<!--           <div class="col-lg-3 col-md-4"> -->
<!--             <div class="gallery-item"> -->
<!--               <a href="assets/img/gallery/gallery-8.jpg" class="gallery-lightbox" data-gall="gallery-item"> -->
<!--                 <img src="assets/img/gallery/gallery-8.jpg" alt="" class="img-fluid"> -->
<!--               </a> -->
<!--             </div> -->
<!--           </div> -->

        </div>
      </div>
    </section>
    
  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>
</html>